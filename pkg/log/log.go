package log

import (
	"os"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

func SetupLogging(verbose, jsonLog bool, name string) *zap.Logger {
	// Set log format.
	var (
		zapEncoder       zapcore.Encoder
		zapEncoderConfig zapcore.EncoderConfig
	)

	// Set log verbosity.
	var zapLevel zapcore.Level

	if verbose {
		zapLevel = zapcore.DebugLevel
		zapEncoderConfig = zap.NewDevelopmentEncoderConfig()
	} else {
		zapLevel = zapcore.InfoLevel
		zapEncoderConfig = zap.NewDevelopmentEncoderConfig()
	}

	if jsonLog {
		zapEncoder = zapcore.NewJSONEncoder(zapEncoderConfig)
	} else {
		zapEncoder = zapcore.NewConsoleEncoder(zapEncoderConfig)
	}

	return zap.New(zapcore.NewCore(zapEncoder, os.Stdout, zapLevel)).Named(name)

}
