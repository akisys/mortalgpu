package gpumgr

import (
	"regexp"
	"strings"

	"go.uber.org/zap"
	v1core "k8s.io/api/core/v1"
)

type ContainerDevice struct {
	gpuDevice       *GpuDevice
	allocatedShares int32
}

func (cd *ContainerDevice) GetGPUDevice() *GpuDevice  { return cd.gpuDevice }
func (cd *ContainerDevice) GetAllocatedShares() int32 { return cd.allocatedShares }
func (cd *ContainerDevice) GetCopy() *ContainerDevice {
	return &ContainerDevice{
		gpuDevice:       cd.gpuDevice.GetCopy(),
		allocatedShares: cd.allocatedShares,
	}
}

type GpuContainer struct {
	containerId       string
	containerName     string
	podId             string
	podNamespace      string
	podMetagpuRequest int64
	podMetagpuLimit   int64
	resourceName      string
	nodename          string
	processes         []*GpuProcess
	devices           []*ContainerDevice
}

func (g *GpuContainer) GetContainerId() string         { return g.containerId }
func (g *GpuContainer) GetContainerName() string       { return g.containerName }
func (g *GpuContainer) GetPodId() string               { return g.podId }
func (g *GpuContainer) GetPodNamespace() string        { return g.podNamespace }
func (g *GpuContainer) GetPodMetaGPURequest() int64    { return g.podMetagpuRequest }
func (g *GpuContainer) GetPodMetaGPULimit() int64      { return g.podMetagpuLimit }
func (g *GpuContainer) GetResourceName() string        { return g.resourceName }
func (g *GpuContainer) GetNodeName() string            { return g.nodename }
func (g *GpuContainer) GetProcesses() []*GpuProcess    { return g.processes }
func (g *GpuContainer) GetDevices() []*ContainerDevice { return g.devices }
func (g *GpuContainer) GetCopy() *GpuContainer {
	var (
		processes = make([]*GpuProcess, len(g.processes))
		devices   = make([]*ContainerDevice, len(g.devices))
	)

	for i, p := range g.processes {
		processes[i] = p.GetCopy()
	}

	for i, d := range g.devices {
		devices[i] = d.GetCopy()
	}

	return &GpuContainer{
		containerId:       g.containerId,
		containerName:     g.containerName,
		podId:             g.podId,
		podNamespace:      g.podNamespace,
		podMetagpuRequest: g.podMetagpuRequest,
		podMetagpuLimit:   g.podMetagpuLimit,
		resourceName:      g.resourceName,
		nodename:          g.nodename,
		processes:         processes,
		devices:           devices,
	}
}

var reMetaDevicePrefix = regexp.MustCompile(`mortalgpu-meta-\d+-\d+-`)

func getContainerId(logger *zap.Logger, pod *v1core.Pod, containerName string) (containerId string) {
	logger = logger.Named("getContainerId")

	for _, status := range pod.Status.ContainerStatuses {
		if status.Name == containerName {
			idx := strings.Index(status.ContainerID, "//")
			if idx != -1 {
				logger.Debug("container id",
					zap.String("pod_name", pod.Name),
					zap.String("pod_namespace", pod.Namespace),
					zap.String("containerId", status.ContainerID[idx+2:]))
				return status.ContainerID[idx+2:]
			} else {
				logger.Error("can't extract container id",
					zap.String("pod_name", pod.Name),
					zap.String("pod_namespace", pod.Namespace))
			}
		}
	}
	return
}

func (c *GpuContainer) setAllocatedGpus(logger *zap.Logger, deviceIDs []string, gpuDevices []*GpuDevice) {
	logger = logger.Named("setAllocatedGpus")

	gpuAllocationMap := make(map[string]int32)

	// Summarize allocated MetaGPU shares per GPU UUID.
	for _, metaDeviceId := range deviceIDs {
		deviceUuid := strings.TrimSuffix(reMetaDevicePrefix.ReplaceAllString(metaDeviceId, ""), "\n")
		if _, ok := gpuAllocationMap[deviceUuid]; ok {
			gpuAllocationMap[deviceUuid] = gpuAllocationMap[deviceUuid] + 1
		} else {
			gpuAllocationMap[deviceUuid] = 1
		}
	}

	if l := logger.Check(zap.DebugLevel, "gpuAllocationMap"); l != nil {
		l.Write(zap.Any("gpuAllocationMap", gpuAllocationMap))
	}

	for uuid, allocatedShares := range gpuAllocationMap {
		for _, device := range gpuDevices {
			if device.uuid == uuid {
				c.devices = append(c.devices, &ContainerDevice{
					gpuDevice:       device,
					allocatedShares: allocatedShares,
				})
			}
		}
	}

	if l := logger.Check(zap.DebugLevel, "devices"); l != nil {
		l.Write(zap.Array("ContainerDevice_s", GpuDeviceArray(gpuDevices)))
	}
}

func NewGpuContainer(logger *zap.Logger,
	containerId, containerName, podId, ns, resourceName, nodename string,
	metagpuRequests, metagpuLimits int64,
	deviceIDs []string,
	gpuDevices []*GpuDevice,
) *GpuContainer {
	p := &GpuContainer{
		containerId:       containerId,
		podId:             podId,
		containerName:     containerName,
		podNamespace:      ns,
		podMetagpuRequest: metagpuRequests,
		podMetagpuLimit:   metagpuLimits,
		resourceName:      resourceName,
		nodename:          nodename,
	}

	logger = logger.With(
		zap.String("container_id", p.containerId),
		zap.String("pod_id", p.podId),
		zap.String("container_name", p.containerName),
		zap.String("pod_namespace", p.podNamespace),
		zap.String("node_name", p.nodename)).
		Named("GpuContainer")

	logger.Debug("NewGpuContainer",
		zap.String("resource_name", p.resourceName),
		zap.Int64("pod_metagpu_request", p.podMetagpuRequest),
		zap.Int64("pod_metagpu_limit", p.podMetagpuLimit))

	// discover allocated GPUs
	p.setAllocatedGpus(logger, deviceIDs, gpuDevices)
	return p
}
