package gpumgr

import (
	"testing"

	"github.com/onsi/ginkgo"
	"github.com/onsi/gomega"
	"github.com/spf13/viper"
	"go.uber.org/zap"
)

func TestAllocator(t *testing.T) {
	viper.SetConfigName("config")
	viper.SetConfigType("yaml")
	viper.AddConfigPath("../../config/")

	logger, err := zap.NewDevelopment()
	if err != nil {
		panic(err)
	}

	if err := viper.ReadInConfig(); err != nil {
		logger.Fatal("config file not found", zap.Error(err))
	}

	gomega.RegisterFailHandler(ginkgo.Fail)
	ginkgo.RunSpecs(t, "Enforcer Suite")
}

var _ = ginkgo.Describe("enforcer", func() {
	ginkgo.Context("enforce", func() {
		logger, err := zap.NewDevelopment()
		if err != nil {
			panic(err)
		}

		ginkgo.It("not oom", func() {
			mgr := &GpuMgr{logger: logger}
			mgr.setGpuDevices()
			if len(mgr.gpuDevices) == 0 {
				logger.Fatal("no gpu devices detected, can't continue unit testing")
			}
			mgr.gpuContainers = map[string][]*GpuContainer{
				"gpu-adapter-1": {
					{
						podMetagpuRequest: 1,
						podMetagpuLimit:   1,
						processes: []*GpuProcess{
							{
								pid:            100,
								deviceUuid:     mgr.gpuDevices[0].uuid,
								gpuUtilization: 0,
								gpuMemBytes:    mgr.gpuDevices[0].memory.ShareSize,
							},
						},
					},
				},
			}

			res := mgr.enforce(logger)
			gomega.Expect(len(res)).To(gomega.Equal(0))
		})

		ginkgo.It("oom", func() {
			mgr := &GpuMgr{logger: logger}
			mgr.setGpuDevices()
			if len(mgr.gpuDevices) == 0 {
				logger.Fatal("no gpu devices detected, can't continue unit testing")
			}

			mgr.gpuContainers = map[string][]*GpuContainer{
				"gpu-adapter-1": {
					{
						podMetagpuRequest: 1,
						podMetagpuLimit:   1,
						processes: []*GpuProcess{
							{
								pid:            100,
								deviceUuid:     mgr.gpuDevices[0].uuid,
								gpuUtilization: 0,
								gpuMemBytes:    mgr.gpuDevices[0].memory.ShareSize,
							},
						},
					},
				},
			}

			res := mgr.enforce(logger)
			gomega.Expect(len(res)).To(gomega.Equal(1))
		})

		ginkgo.It("false positive oom", func() {
			mgr := &GpuMgr{logger: logger}
			mgr.setGpuDevices()
			if len(mgr.gpuDevices) == 0 {
				logger.Fatal("no gpu devices detected, can't continue unit testing")
			}

			mgr.gpuContainers = map[string][]*GpuContainer{
				"gpu-adapter-1": {
					{
						podMetagpuRequest: 1,
						podMetagpuLimit:   1,
						processes: []*GpuProcess{
							{
								pid:            100,
								deviceUuid:     mgr.gpuDevices[0].uuid,
								gpuUtilization: 0,
								gpuMemBytes:    mgr.gpuDevices[0].memory.ShareSize,
							},
						},
					},
				},
			}

			res := mgr.enforce(logger)
			gomega.Expect(len(res)).To(gomega.Equal(0))
		})
	})
})
