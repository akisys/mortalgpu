package gpumgr

import (
	"path/filepath"
	"regexp"

	"github.com/prometheus/procfs"
	"github.com/shirou/gopsutil/v3/process"
	"go.uber.org/zap"
)

type GpuProcess struct {
	pid uint32
	// deviceUuid - Parent device UUID if MIG device, usual device UUID if not MIG mode.
	deviceUuid string
	// metaGpuUuid - MIG device UUID, if MIG mode, otherwise a normal device UUID.
	metaGpuUuid       string
	gpuUtilization    uint32
	gpuMemBytes       uint64
	cmdline           []string
	user              string
	containerId       string
	gpuInstanceId     int32
	computeInstanceId int32
	migInstanceID     int

	logger *zap.Logger
}

func (p *GpuProcess) GetPid() uint32            { return p.pid }
func (p *GpuProcess) GetDeviceUUID() string     { return p.deviceUuid }
func (p *GpuProcess) GetMetaGpuUUID() string    { return p.metaGpuUuid }
func (p *GpuProcess) GetGPUUtilization() uint32 { return p.gpuUtilization }
func (p *GpuProcess) GetGPUMemory() uint64      { return p.gpuMemBytes }
func (p *GpuProcess) GetCMDLine() []string {
	res := make([]string, len(p.cmdline))
	copy(res, p.cmdline)
	return res
}
func (p *GpuProcess) GetUser() string             { return p.user }
func (p *GpuProcess) GetContainerId() string      { return p.containerId }
func (p *GpuProcess) GetGPUInstanceId() int32     { return p.gpuInstanceId }
func (p *GpuProcess) GetComputeInstanceId() int32 { return p.computeInstanceId }
func (p *GpuProcess) GetMigInstanceId() int       { return p.migInstanceID }
func (p *GpuProcess) GetCopy() *GpuProcess {
	cmdline := make([]string, len(p.cmdline))
	copy(cmdline, p.cmdline)

	return &GpuProcess{
		pid:               p.pid,
		deviceUuid:        p.deviceUuid,
		metaGpuUuid:       p.metaGpuUuid,
		gpuUtilization:    p.gpuUtilization,
		gpuMemBytes:       p.gpuMemBytes,
		cmdline:           cmdline,
		user:              p.user,
		containerId:       p.containerId,
		gpuInstanceId:     p.gpuInstanceId,
		computeInstanceId: p.computeInstanceId,
		migInstanceID:     p.migInstanceID,
		logger:            p.logger,
	}
}

var scopeContainerCgroupRe = regexp.MustCompile(`[^-]+-(.+)\.scope`)

func (p *GpuProcess) SetProcessCmdline() {
	if pr, err := process.NewProcess(int32(p.pid)); err == nil {
		var e error
		p.cmdline, e = pr.CmdlineSlice()
		if e != nil {
			p.logger.Named("SetProcessCmdline").Error("can not get process cmdline", zap.Error(err))
		} else {
			p.logger.Named("SetProcessCmdline").Debug("Process cmdline is set")
			p.logger = p.logger.With(zap.Strings("process_cmdline", p.cmdline))
		}
	} else {
		p.logger.Named("SetProcessCmdline").Error("can not get process cmdline", zap.Error(err))
	}
}

func (p *GpuProcess) SetProcessUsername() {
	if pr, err := process.NewProcess(int32(p.pid)); err == nil {
		var e error
		p.user, e = pr.Username()
		if e != nil {
			p.logger.Named("SetProcessUsername").Warn("can not get process username", zap.Error(e))
		} else {
			p.logger.Named("SetProcessUsername").Debug("process username is set")
			p.logger = p.logger.With(zap.String("process_username", p.user))
		}
	} else {
		p.logger.Named("SetProcessUsername").Warn("can not get process username", zap.Error(err))
	}
}

func (p *GpuProcess) Kill() error {
	p.logger.Named("Kill").Debug("Killing the process")

	if pr, err := process.NewProcess(int32(p.pid)); err == nil {
		return pr.Kill()
	} else {
		return err
	}
}

func (p *GpuProcess) SetProcessContainerId() {
	logger := p.logger.Named("SetProcessContainerId")

	if proc, err := procfs.NewProc(int(p.pid)); err == nil {
		var e error
		var cgroups []procfs.Cgroup
		cgroups, e = proc.Cgroups()
		if e != nil {
			logger.Error("can not get process cgroups", zap.Error(err))
		}
		if len(cgroups) == 0 {
			logger.Error("cgroups list is empty")
		} else {
			if l := logger.Check(zap.DebugLevel, "process cgroups"); l != nil {
				l.Write(zap.Any("cgroups", cgroups))
			}
		}

	ExitContainerIdSet:
		var processCGroupName string

		if p.containerId == "" {
			for _, g := range cgroups {
				for _, c := range g.Controllers {
					if c == "memory" {
						processCGroupName = filepath.Base(g.Path)

						if logger.Core().Enabled(zap.DebugLevel) {
							logger = logger.With(zap.String("process_cgroup_name", processCGroupName))
							logger.Debug("Process cgroup name")
						}

						// Docker-based runtime container cgroup name is "docker-<ContainerId>.scope"
						// CRIO runtime container cgroup name is "crio-<ContainerId>.scope"
						// Containerd runtime container cgroup name is just "<ContainerId>"
						scopeContainerMatch := scopeContainerCgroupRe.FindStringSubmatch(processCGroupName)

						// The scopeContainerCgroupRe regex has
						// only one capture group, so the match's len must be 2
						// and the match with index 1 is the captured value.
						if len(scopeContainerMatch) == 2 {
							// Extract cgroup name (CRIO and Docker prefix the cgroup name).
							p.containerId = scopeContainerMatch[1]

							if logger.Core().Enabled(zap.DebugLevel) {
								logger = logger.With(zap.String("process_container_id", p.containerId))
								logger.Debug("Process cgroup name matching the <runtime>-<id>.scope pattern, assigning container ID for pid")
							}
						} else {
							// Use full cgroup name (containerd case).
							p.containerId = processCGroupName

							if logger.Core().Enabled(zap.DebugLevel) {
								logger = logger.With(zap.String("process_container_id", p.containerId))
								logger.Debug("Process cgroup does not match the <runtime>-<id>.scope pattern, assigning the full cgroup name for pid")
							}
						}

						goto ExitContainerIdSet
					}
				}
			}

			logger.Warn("unable to set containerId for pid")
		} else {
			logger.Debug("Container ID has been set for process")
			p.logger = p.logger.With(
				zap.String("process_container_id", p.containerId),
				zap.String("process_cgroup_name", processCGroupName),
			)
		}
	}
}

func (p *GpuProcess) GetShortCmdLine() string {
	if len(p.cmdline) == 0 {
		return "-"
	}
	return p.cmdline[0]
}

// func (p *GpuProcess) GetDevice(devices []*GpuDevice) *GpuDevice {
// 	for _, device := range devices {
// 		if device.DeviceUUID == p.DeviceUuid {
// 			return device
// 		}
// 	}
// 	return nil
// }

func NewGpuProcess(logger *zap.Logger, pid, gpuUtil uint32, gpuMemBytes uint64, mgpuUuid, devUuid string, migInstanceId int, computeInstanceID, gpuInstanceID int32) *GpuProcess {
	p := &GpuProcess{
		pid:               pid,
		gpuUtilization:    gpuUtil,
		gpuMemBytes:       gpuMemBytes,
		metaGpuUuid:       mgpuUuid,
		deviceUuid:        devUuid,
		gpuInstanceId:     gpuInstanceID,
		computeInstanceId: computeInstanceID,

		logger: logger.Named("NewGpuProcess"),
	}

	p.logger.Debug("Creating new Gpu process")

	p.SetProcessUsername()
	p.SetProcessCmdline()
	p.SetProcessContainerId()
	return p
}
