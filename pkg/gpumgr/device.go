package gpumgr

import (
	"os"

	"github.com/NVIDIA/go-nvml/pkg/nvml"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"

	"gitlab.com/MaxIV/kubernetes/mortalgpu/pkg/sharecfg"
)

type DeviceMemory struct {
	// Total - memory, bytes.
	Total uint64
	// Free - memory, bytes.
	Free uint64
	// Used - memory, bytes.
	Used uint64
	// ShareSize - memory, bytes.
	ShareSize uint64
}

var _ zapcore.ObjectMarshaler = (*DeviceMemory)(nil)

func (d *DeviceMemory) MarshalLogObject(enc zapcore.ObjectEncoder) error {
	enc.AddUint64("total_bytes", d.Total)
	enc.AddUint64("free_bytes", d.Free)
	enc.AddUint64("used_bytes", d.Used)
	enc.AddUint64("share_size_bytes", d.ShareSize)

	return nil
}

type DeviceUtilization struct {
	Gpu    uint32
	Memory uint32
}

var _ zapcore.ObjectMarshaler = (*DeviceUtilization)(nil)

func (d *DeviceUtilization) MarshalLogObject(enc zapcore.ObjectEncoder) error {
	enc.AddUint32("gpu", d.Gpu)
	enc.AddUint32("memory", d.Memory)

	return nil
}

// GpuDeviceArray just a typedef to implement zapcore.ArrayMarshaler.
type GpuDeviceArray []*GpuDevice

var _ zapcore.ArrayMarshaler = (GpuDeviceArray)(nil)

func (ga GpuDeviceArray) MarshalLogArray(enc zapcore.ArrayEncoder) error {
	for _, d := range ga {
		_ = enc.AppendObject(d)
	}

	return nil
}

// GPUMigInstanceIDNoMIG constant value to indicate that the device is not using MIG.
const GPUMigInstanceIDNoMIG = -1

type GpuDevice struct {
	// uuid - MIG device UUID, if MIG mode, otherwise a normal device UUID.
	uuid string
	// DeviceUUID - Parent device UUID if MIG device, usual device UUID if not MIG mode.
	deviceUUID      string
	deviceIndex     int
	migId           int
	shares          int
	allocatedShares int
	resourceName    string
	utilization     DeviceUtilization
	memory          DeviceMemory
	nodename        string

	logger *zap.Logger
}

// GetUUID - MIG device UUID, if MIG mode, otherwise a normal device UUID.
func (d *GpuDevice) GetUUID() string { return d.uuid }

// GetDeviceUUID - Parent device UUID if MIG device, usual device UUID if not MIG mode.
func (d *GpuDevice) GetDeviceUUID() string { return d.deviceUUID }

func (d *GpuDevice) GetDeviceIndex() int               { return d.deviceIndex }
func (d *GpuDevice) GetMigId() int                     { return d.migId }
func (d *GpuDevice) IsMigDevice() bool                 { return d.migId != GPUMigInstanceIDNoMIG }
func (d *GpuDevice) GetShares() int                    { return d.shares }
func (d *GpuDevice) GetAllocatedShares() int           { return d.allocatedShares }
func (d *GpuDevice) GetResourceName() string           { return d.resourceName }
func (d *GpuDevice) GetUtilization() DeviceUtilization { return d.utilization }
func (d *GpuDevice) GetMemory() DeviceMemory           { return d.memory }
func (d *GpuDevice) GetNodeName() string               { return d.nodename }
func (d *GpuDevice) GetCopy() *GpuDevice {
	return &GpuDevice{
		uuid:            d.uuid,
		deviceUUID:      d.deviceUUID,
		deviceIndex:     d.deviceIndex,
		migId:           d.migId,
		shares:          d.shares,
		allocatedShares: d.allocatedShares,
		resourceName:    d.resourceName,
		utilization:     d.utilization,
		memory:          d.memory,
		nodename:        d.nodename,
		logger:          d.logger,
	}
}

var _ zapcore.ObjectMarshaler = (*GpuDevice)(nil)

func (d *GpuDevice) MarshalLogObject(enc zapcore.ObjectEncoder) error {
	enc.AddString("uuid", d.uuid)
	enc.AddString("device_uuid", d.deviceUUID)
	enc.AddInt("device_index", d.deviceIndex)
	enc.AddInt("mig_id", d.migId)
	enc.AddInt("shares", d.shares)
	enc.AddInt("allocated_shares", d.allocatedShares)
	enc.AddString("resource_name", d.resourceName)
	_ = enc.AddObject("utilization", &d.utilization)
	_ = enc.AddObject("memory", &d.memory)
	enc.AddString("node_name", d.nodename)

	return nil
}

func NewGpuDevice(logger *zap.Logger, miguuid, deviceuuid string, index, miginstanceid int, utilization nvml.Utilization) *GpuDevice {
	d := &GpuDevice{
		uuid:        miguuid,
		deviceUUID:  deviceuuid,
		deviceIndex: index,
		migId:       miginstanceid,
		utilization: DeviceUtilization{Gpu: utilization.Gpu, Memory: utilization.Memory / uint32(MB)},

		logger: logger.Named("GpuDevice").With(
			zap.String("gpu_device_uuid", deviceuuid),
			zap.String("gpu_device_mig_uuid", miguuid),
			zap.Int("gpu_device_index", index),
			zap.Int("gpu_device_mig_index", miginstanceid),
		),
	}

	if l := logger.Check(zap.DebugLevel, "NewGPUDevice"); l != nil {
		l.Write(zap.Uint32("gpu_utilization_memory", d.utilization.Memory), zap.Uint32("gpu_utilization_gpu", d.utilization.Gpu))
	}

	// set nodename
	d.setNodename()
	return d
}

func (d *GpuDevice) setNodename() {
	hostname, err := os.Hostname()
	if err != nil {
		d.logger.Error("failed to detect hostname", zap.Error(err))
	}

	d.logger = d.logger.With(zap.String("node_hostname", hostname))

	d.nodename = hostname
}

func (d *GpuDevice) setGpuShareConfigs() bool {
	deviceSharingConfigs := sharecfg.NewDeviceSharingConfig(d.logger)
	if deviceSharing, err := deviceSharingConfigs.GetDeviceSharingConfigs(d.deviceUUID, d.migId); err != nil {
		if l := d.logger.Check(zap.DebugLevel, "unable to find sharing configs for device"); l != nil {
			l.Write(zap.Error(err))
		}
		return false
	} else {
		d.shares = deviceSharing.MetagpusPerGpu
		d.resourceName = deviceSharing.ResourceName

		// Enrich logger.
		d.logger = d.logger.With(zap.String("gpu_resource_name", d.resourceName), zap.Int("gpu_metagpu_shares", d.shares))

		d.logger.Debug("sharing configs for device")

		return true
	}
}

func (d *GpuDevice) setGpuMemoryUsage(memory nvml.Memory) {
	d.memory = DeviceMemory{
		Total:     memory.Total,
		Free:      memory.Free,
		Used:      memory.Used,
		ShareSize: memory.Total / uint64(d.shares),
	}
}

// getDeviceLogger returns logger enriched with the device-related fields.
func (d *GpuDevice) getDeviceLogger() *zap.Logger {
	return d.logger
}
