// Package gpumgr implements MortalGPUs detection, processes accounting and
// memory limits enforcement.
package gpumgr

import (
	"context"
	"fmt"
	"os"
	"strconv"
	"sync"
	"time"

	"github.com/spf13/viper"
	"go.uber.org/zap"
	v1core "k8s.io/api/core/v1"

	"gitlab.com/MaxIV/kubernetes/mortalgpu/pkg/nvmlutils"
	"gitlab.com/MaxIV/kubernetes/mortalgpu/pkg/podapi"
	"gitlab.com/MaxIV/kubernetes/mortalgpu/pkg/podmonitor"
	"gitlab.com/MaxIV/kubernetes/mortalgpu/pkg/sharecfg"
)

const MB uint64 = 1024 * 1024

type GpuMgr struct {
	containerLevelVisibilityToken string
	deviceLevelVisibilityToken    string
	gpuDevices                    []*GpuDevice
	// map of gpu containers, i.e. a container + allocated GPU + resources data.
	// It is possible that one Kubernetes container will be multiple times in the
	// map because a container is added for each GPU resource.
	// One should use GpuContainer.GetResourceName() to get the name of the GPU.
	// Key is the GPU Kubernetes resource name.
	gpuContainers map[string][]*GpuContainer

	// collection of the gpu processes: the anonymous and active running
	gpuContainersCollector map[string][]*GpuContainer

	logger *zap.Logger

	mtx sync.RWMutex
}

type GpuDeviceInfo struct {
	Node     string
	Metadata map[string]string
	Devices  []*GpuDevice
}

func (m *GpuMgr) startGpuStatusCache() {
	go func() {
		for {
			time.Sleep(5 * time.Second)

			// This "global" lock during the update procedure is not optimal
			// but might work somewhat well if not too high throughput.
			m.mtx.Lock()

			// set gpu devices
			m.setGpuDevices()
			// set gpu containers
			m.discoverGpuContainers()
			// set active gpu processes
			m.enrichGpuContainer()
			// set per physical GPU allocation statistics.
			m.updateAllocatedShares()
			// set final gpu containers list
			m.setGpuContainers()

			m.mtx.Unlock()

			// Print summary.
			m.logSummary()
		}
	}()
}

func (m *GpuMgr) logSummary() {
	if m.logger.Core().Enabled(zap.InfoLevel) {
		logger := m.logger.Named("logSummary")

		m.mtx.RLock()

		for resourceName, containers := range m.gpuContainers {
			iLog := logger.With(zap.String("resource_name", resourceName))

			for _, c := range containers {
				if iLog.Core().Enabled(zap.DebugLevel) {
					processes := debugFormatZapGPUProcesses(c.GetProcesses())

					iLog.Debug("GPU container verbose summary",
						zap.String("pod_namespace", c.podNamespace),
						zap.String("pod_name", c.podId),
						zap.String("container_name", c.containerName),
						zap.String("resource_name", c.resourceName),
						zap.Int64("gpu_request", c.podMetagpuRequest),
						zap.Int64("gpu_limit", c.podMetagpuLimit),
						zap.String("container_id", c.containerId),
						zap.String("node_name", c.nodename),
						zap.Any("processes", processes),
					)
				} else {
					iLog.Info("GPU container summary",
						zap.String("pod_namespace", c.podNamespace),
						zap.String("pod_name", c.podId),
						zap.String("container_name", c.containerName),
						zap.String("resource_name", c.resourceName),
						zap.Int64("gpu_request", c.podMetagpuRequest),
						zap.Int64("gpu_limit", c.podMetagpuLimit),
						zap.String("container_id", c.containerId),
						zap.String("node_name", c.nodename),
					)
				}
			}
		}

		m.mtx.RUnlock()
	}
}

func (m *GpuMgr) setGpuDevices() {
	logger := m.logger.Named("setGpuDevices")

	var gpuDevices []*GpuDevice

	for _, device := range nvmlutils.GetDevices(logger) {
		uuid := nvmlutils.GetDeviceUUID(logger, device)
		idx := nvmlutils.GetDeviceIndex(logger, device)

		iLogger := logger.With(zap.String("nvml_device_uuid", uuid), zap.Int("nvml_device_index", idx))

		// create GPUDevice for each MIG if running in MIG mode
		if nvmlutils.IsMigDevice(logger, device) {
			iLogger = iLogger.With(zap.Bool("is_mig_device", true))

			iLogger.Debug("Device is a MIG device, getting child MIG devices")

			for _, migdevice := range nvmlutils.GetMigDevices(iLogger, device) {
				migidx := nvmlutils.GetMigInstanceId(iLogger, migdevice)
				miguuid := nvmlutils.GetDeviceUUID(iLogger, migdevice)
				deviceMemory := nvmlutils.GetDeviceMemory(iLogger, migdevice)
				utilization := nvmlutils.GetUtilizationRates(iLogger, device)

				// create new MetaGPU device and manage is there is share config for it
				metaGpuDevice := NewGpuDevice(iLogger, miguuid, uuid, idx, migidx, *utilization)
				if metaGpuDevice.setGpuShareConfigs() {
					metaGpuDevice.setGpuMemoryUsage(*deviceMemory)
					gpuDevices = append(gpuDevices, metaGpuDevice)

					// Debug device config.
					if l := iLogger.Check(zap.DebugLevel, "Meta GPU device configured"); l != nil {
						l.Write(zap.Any("device_config", metaGpuDevice))
					}
				}
			}
		} else {
			iLogger = iLogger.With(zap.Bool("is_mig_device", false))

			iLogger.Debug("Device is not a MIG device, working with entire GPU as GPUDevice")

			// otherwise work with entire GPU as GPUDevice
			deviceMemory := nvmlutils.GetDeviceMemory(iLogger, device)
			utilization := nvmlutils.GetUtilizationRates(iLogger, device)
			// create new MetaGPU device and manage is there is share config for it
			metaGpuDevice := NewGpuDevice(iLogger, uuid, uuid, idx, GPUMigInstanceIDNoMIG, *utilization)
			if metaGpuDevice.setGpuShareConfigs() {
				metaGpuDevice.setGpuMemoryUsage(*deviceMemory)
				gpuDevices = append(gpuDevices, metaGpuDevice)

				// Debug device config.
				if l := iLogger.Check(zap.DebugLevel, "Meta GPU device configured"); l != nil {
					l.Write(zap.Any("device_config", metaGpuDevice))
				}
			}
		}
	}

	if logger.Core().Enabled(zap.DebugLevel) {
		debugLogGPUDevices(m.logger.Named("setGpuDevices"), "Collected GPU devices", gpuDevices)
	}

	m.gpuDevices = gpuDevices
}

func (m *GpuMgr) enrichGpuContainer() {
	if m.logger.Core().Enabled(zap.DebugLevel) {
		debugLogGPUDevices(m.logger.Named("enrichGpuContainer"), "Starting: Matching running processes with GPU devices", m.gpuDevices)
	}

	for _, device := range m.gpuDevices {
		logger := device.getDeviceLogger().Named("enrichGpuContainer")

		logger.Debug("Getting running processes for a GPU Device")

		// Bug: It is supposed to be called with device.MigId but for some reason
		// the call fails with error in GetComputeRunningProcesses => getMigDeviceByIdx => nvml.DeviceGetMigDeviceHandleByIndex
		// so as a workaround we don't use MIG here.
		processes := nvmlutils.GetComputeRunningProcesses(logger, device.GetDeviceIndex(), GPUMigInstanceIDNoMIG)
		if l := logger.Check(zap.DebugLevel, "got GPU processes"); l != nil {
			l.Write(zap.Any("processes", processes))
		}

		for _, nvmlProcessInfo := range processes {
			iLogger := logger.With(
				zap.Uint32("process_pid", nvmlProcessInfo.Pid),
				zap.Uint32("process_gpu_instance_id", nvmlProcessInfo.GpuInstanceId),
				zap.Uint32("process_compute_instance_id", nvmlProcessInfo.ComputeInstanceId),
				zap.Uint64("process_used_gpu_memory_bytes", nvmlProcessInfo.UsedGpuMemory),
			)

			// skip if process does not belong to this MIG instance
			if device.IsMigDevice() && device.migId != int(nvmlProcessInfo.GpuInstanceId) {
				if l := iLogger.Check(zap.DebugLevel, "Compute running process does not belong to this device"); l != nil {
					l.Write()
				}

				continue
			} else {
				if l := iLogger.Check(zap.DebugLevel, "Found a GPU on which the process is running"); l != nil {
					l.Write()
				}
			}

			var gpuProc *GpuProcess
			// MIG devices do not support accounting yet (2023, December, 06),
			// so the CPU utilization is set to 0.
			if device.IsMigDevice() {
				gpuProc = NewGpuProcess(iLogger,
					nvmlProcessInfo.Pid, 0, nvmlProcessInfo.UsedGpuMemory,
					device.uuid, device.deviceUUID,
					device.migId, int32(nvmlProcessInfo.ComputeInstanceId), int32(nvmlProcessInfo.GpuInstanceId))
			} else {
				stats := nvmlutils.GetAccountingStats(iLogger, device.GetDeviceIndex(), nvmlProcessInfo.Pid)

				gpuProc = NewGpuProcess(iLogger,
					nvmlProcessInfo.Pid, stats.GpuUtilization, nvmlProcessInfo.UsedGpuMemory,
					device.uuid, device.deviceUUID,
					device.migId, int32(nvmlProcessInfo.ComputeInstanceId), int32(nvmlProcessInfo.GpuInstanceId))
			}

			for _, containers := range m.gpuContainersCollector {
				for _, c := range containers {
					if iLogger.Core().Enabled(zap.DebugLevel) {
						iLogger.With(zap.String("containers_collector_container_name", c.containerName),
							zap.String("containers_collector_pod_namespace", c.podNamespace),
							zap.String("containers_collector_container_id", c.containerId),
							zap.String("containers_collector_pod_id", c.podId),
							zap.String("gpu_process_container_id", gpuProc.containerId),
							zap.Bool("is_match", c.containerId == gpuProc.containerId),
							zap.Any("process_info", debugFormatZapGPUProcess(gpuProc))).
							Debug("Matching a GPU process with a GPU container")
					}

					if c.containerId == gpuProc.containerId {
						c.processes = append(c.processes, gpuProc)
					}
				}
			}
		}
	}
}

func (m *GpuMgr) updateAllocatedShares() {
	logger := m.logger.Named("updateAllocatedShares")

	stats := make(map[string]int, len(m.gpuDevices))

	for _, containers := range m.gpuContainersCollector {
		for _, c := range containers {
			var l *zap.Logger
			if logger.Core().Enabled(zap.DebugLevel) {
				l = logger.With(
					zap.String("container_id", c.containerId),
					zap.String("container_name", c.containerName),
					zap.String("pod_name", c.podId),
					zap.String("pod_namespace", c.podNamespace),
					zap.String("node_name", c.nodename))
				l.Debug("collecting allocation data for a container")
			}

			for _, d := range c.devices {
				if logger.Core().Enabled(zap.DebugLevel) {
					l.Debug("container GPU device",
						zap.String("parent_device_uuid", d.gpuDevice.deviceUUID),
						zap.String("device_uuid", d.gpuDevice.uuid),
						zap.Int32("allocated_shares", d.allocatedShares))
				}

				alloc, ok := stats[d.gpuDevice.uuid]
				if !ok {
					alloc = int(d.allocatedShares)
				} else {
					alloc += int(d.allocatedShares)
				}

				stats[d.gpuDevice.uuid] = alloc
			}
		}
	}

	for _, d := range m.gpuDevices {
		d.allocatedShares = stats[d.uuid]

		if logger.Core().Enabled(zap.DebugLevel) {
			logger.Debug("GPU allocated statistics",
				zap.String("device_uuid", d.uuid),
				zap.Int("allocated_shares", d.allocatedShares))
		}
	}
}

func (m *GpuMgr) setGpuContainers() {
	m.gpuContainers = m.gpuContainersCollector

	if l := m.logger.Named("setGpuContainers").Check(zap.DebugLevel, "discovered GPU containers"); l != nil {
		l.Write(zap.Int("count", len(m.gpuContainers)))
	}
}

func (m *GpuMgr) discoverGpuContainers() {
	logger := m.logger.Named("discoverGpuContainers")

	// Get Pods.
	c, err := podapi.GetK8sClient(logger)
	if err != nil {
		logger.Error("can not init K8S client", zap.Error(err))
		return
	}

	// ToDo: investigate whether the List operation is optimal or we should set "resource version"
	// to query "Any" version and not query ALL KubeAPI servers to find the
	// cluster-consistent view.
	pl := &v1core.PodList{}
	if err := c.List(context.Background(), pl); err != nil {
		logger.Error("can not list Pods", zap.Error(err))
		return
	}

	// Load device sharing config.
	cfg := sharecfg.NewDeviceSharingConfig(logger)

	deviceResourceNames := make([]v1core.ResourceName, 0, len(cfg.Configs))
	for _, c := range cfg.Configs {
		deviceResourceNames = append(deviceResourceNames, v1core.ResourceName(c.ResourceName))
	}

	// Get container devices.
	podmon := podmonitor.New()
	if err := podmon.Load(context.Background(), logger, deviceResourceNames...); err != nil {
		logger.Error("Can not load PodMonitor Container Devices", zap.Error(err))
		return
	}

	hostNodeName := viper.GetString("nodename")

	// reset gpu containers collector
	m.gpuContainersCollector = make(map[string][]*GpuContainer)

	for _, p := range pl.Items {
		for _, container := range p.Spec.Containers {
			for _, resourceName := range deviceResourceNames {
				requests, isreq := container.Resources.Requests[resourceName]
				if limits, ok := container.Resources.Limits[resourceName]; ok {
					// backward compatible logic: if no requests assume it is equal to limits
					if !isreq {
						requests = limits
					}
					limitShares := limits.Value()
					// as of now, kubernetes does not allow overcommit for custom resources
					// it enforces requests must be equal to limits and refuse to schhedule
					// the pod othervise
					// to mitigate this issue and allow overcommit, the cutoff limit
					// can be defined via Pod annotation
					annotationKey := ("gpu-mem-limit." + resourceName).String()

					if annotationValue, ok := p.ObjectMeta.Annotations[annotationKey]; ok {
						annotationLimit, err := strconv.ParseInt(annotationValue, 10, 64)
						if err != nil {
							logger.Error("Failed to parse memory limit from annotation",
								zap.String("namespace", p.GetNamespace()),
								zap.String("pod_name", p.GetName()),
								zap.String("container_name", container.Name),
								zap.String("resource_name", string(resourceName)),
								zap.String("annotation_key", annotationKey),
								zap.Error(err))
						} else {
							limitShares = annotationLimit
							if l := logger.Check(zap.DebugLevel, "Using memory enforcement limit from annotation"); l != nil {
								l.Write(
									zap.String("namespace", p.GetNamespace()),
									zap.String("pod_name", p.GetName()),
									zap.String("container_name", container.Name),
									zap.String("resource_name", string(resourceName)),
									zap.String("annotation_key", annotationKey),
									zap.Int64("limit_shares", limitShares),
									zap.Int64("request_shares", requests.Value()),
								)
							}
						}
					}

					// Get information about assigned MetaGPU devices.
					deviceIDs, err := podmon.GetDeviceIDs(p.GetNamespace(), p.GetName(), container.Name, resourceName)
					if err != nil {
						logger.Error("Can not GetDeviceIDs",
							zap.String("namespace", p.GetNamespace()),
							zap.String("pod_name", p.GetName()),
							zap.String("container_name", container.Name),
							zap.String("resource_name", string(resourceName)),
							zap.Error(err))
						return
					}

					if hostNodeName == "" || hostNodeName == p.Spec.NodeName {
						rn := resourceName.String()

						m.gpuContainersCollector[rn] = append(
							m.gpuContainersCollector[rn],
							NewGpuContainer(
								logger,
								getContainerId(logger, &p, container.Name),
								container.Name,
								p.Name,
								p.Namespace,
								rn,
								p.Spec.NodeName,
								requests.Value(),
								limitShares,
								deviceIDs,
								m.gpuDevices,
							),
						)
					}
				}
			}
		}
	}
}

func (m *GpuMgr) GetDeviceInfo() GpuDeviceInfo {
	hostname, err := os.Hostname()
	if err != nil {
		m.logger.Named("GetDeviceInfo").Error("failed to detect hostname", zap.Error(err))
	}

	logger := m.logger.Named("GetDeviceInfo")

	info := make(map[string]string)
	cudaVersion := nvmlutils.SystemGetCudaDriverVersion(logger)
	info["cudaVersion"] = fmt.Sprintf("%d", cudaVersion)
	driver := nvmlutils.SystemGetDriverVersion(logger)
	info["driverVersion"] = driver

	// Copy devices.
	m.mtx.RLock()
	devices := make([]*GpuDevice, len(m.gpuDevices))
	for i, d := range m.gpuDevices {
		devices[i] = d.GetCopy()
	}
	m.mtx.RUnlock()

	return GpuDeviceInfo{Node: hostname, Metadata: info, Devices: devices}
}

func (m *GpuMgr) GetProcesses(podId string) map[string][]*GpuContainer {
	m.mtx.RLock()
	defer m.mtx.RUnlock()

	// if podId is set, return single process
	if podId != "" {
		gpuContainers := make(map[string][]*GpuContainer)

		for resourceName, containers := range m.gpuContainers {
			for _, container := range containers {
				if container.podId == podId {
					gpuContainers[resourceName] = append(gpuContainers[resourceName], container.GetCopy())
				}
			}
		}

		return gpuContainers
	}

	// return all processes.
	gpuContainers := make(map[string][]*GpuContainer)

	for resourceName, containers := range m.gpuContainers {
		for _, container := range containers {
			gpuContainers[resourceName] = append(gpuContainers[resourceName], container.GetCopy())
		}
	}

	return gpuContainers
}

func (m *GpuMgr) GetMetaDevices() map[string]*GpuDevice {
	m.mtx.RLock()
	defer m.mtx.RUnlock()

	deviceMap := make(map[string]*GpuDevice, len(m.gpuDevices))

	for _, d := range m.gpuDevices {
		deviceMap[d.uuid] = d.GetCopy()
	}

	return deviceMap
}

func (m *GpuMgr) KillGpuProcess(pid uint32) error {
	logger := m.logger.Named("KillGpuProcess")

	p := NewGpuProcess(logger, pid, 0, 0, "", "", 0, 0, 0)
	return p.Kill()
}

func (m *GpuMgr) SetDeviceLevelVisibilityToken(token string) {
	m.mtx.Lock()
	m.deviceLevelVisibilityToken = token
	m.mtx.Unlock()
}

func (m *GpuMgr) SetContainerLevelVisibilityToken(token string) {
	m.mtx.Lock()
	m.containerLevelVisibilityToken = token
	m.mtx.Unlock()
}

func NewGpuManager(logger *zap.Logger) *GpuMgr {
	mgr := &GpuMgr{logger: logger.Named("GpuManager")}
	// init gpu devices
	mgr.setGpuDevices()
	// init gpu containers
	mgr.discoverGpuContainers()
	// init active gpu processes
	mgr.enrichGpuContainer()
	// set gpu processes
	mgr.setGpuContainers()
	// start gpu devices and processes cache
	mgr.startGpuStatusCache()
	// set per physical GPU allocation statistics.
	mgr.updateAllocatedShares()
	// start mem enforcer
	if viper.GetBool("memoryEnforcer") {
		mgr.StartMemoryEnforcer()
	}
	return mgr
}

func debugLogGPUDevices(logger *zap.Logger, message string, gpuDevices []*GpuDevice) {
	if l := logger.Check(zap.DebugLevel, message); l != nil {
		devices := make(map[string]map[string]any, len(gpuDevices))

		for _, device := range gpuDevices {
			devices[device.resourceName+"-"+device.GetUUID()] = map[string]any{
				"gpu_device_uuid":     device.GetDeviceUUID(),
				"gpu_device_index":    device.GetDeviceIndex(),
				"gpu_device_mig_uuid": device.GetUUID(),
				"gpu_device_mig_id":   device.GetMigId(),
			}
		}

		l.Write(zap.Any("gpu_devices_list", devices))
	}
}

func debugFormatZapGPUProcess(p *GpuProcess) map[string]any {
	return map[string]any{
		"process_pid":                 p.pid,
		"process_cmdline":             p.cmdline,
		"process_container_id":        p.containerId,
		"process_user":                p.user,
		"process_compute_instance_id": p.computeInstanceId,
		"process_gpu_instance_id":     p.gpuInstanceId,
		"process_gpu_device_uuid":     p.deviceUuid,
		"process_gpu_mig_instance_id": p.migInstanceID,
		"process_gpu_memory_bytes":    p.gpuMemBytes,
		"process_gpu_utilization":     p.gpuUtilization,
	}
}

func debugFormatZapGPUProcesses(p []*GpuProcess) []map[string]any {
	procs := make([]map[string]any, 0, len(p))

	for _, v := range p {
		procs = append(procs, debugFormatZapGPUProcess(v))
	}

	return procs
}
