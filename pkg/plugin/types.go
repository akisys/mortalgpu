package plugin

import (
	"context"
	"sync"
	"time"

	"gitlab.com/MaxIV/kubernetes/mortalgpu/pkg/sharecfg"
	"go.uber.org/zap"
	"google.golang.org/grpc"
	pluginapi "k8s.io/kubelet/pkg/apis/deviceplugin/v1beta1"
)

type DeviceManager interface {
	GetPluginDevices(logger *zap.Logger) []*pluginapi.Device
	GetDeviceSharingConfig() *sharecfg.DeviceSharingConfig
	GetUnixSocket() string
	ParseRealDeviceId(logger *zap.Logger, metaDevicesIds []string) (realDeviceId []string)
	MetagpuAllocation(logger *zap.Logger, allocationSize int, availableDevIds []string) ([]string, error)
}

type DeviceUuid string

type MetaGpuDevicePlugin struct {
	DeviceManager
	server               *grpc.Server
	socket               string
	ctx                  context.Context
	ctxCf                context.CancelFunc
	metaGpuRecalculation chan bool

	logger *zap.Logger
}

type NvidiaDeviceManager struct {
	devices  []*MetaDevice
	cacheTTL time.Duration
	shareCfg *sharecfg.DeviceSharingConfig

	mtx sync.RWMutex
}

type MetaDevice struct {
	UUID  string
	Index int
}
