package plugin

import (
	b64 "encoding/base64"
	"fmt"
	"regexp"
	"time"

	"github.com/spf13/viper"
	"gitlab.com/MaxIV/kubernetes/mortalgpu/pkg/allocator"
	"gitlab.com/MaxIV/kubernetes/mortalgpu/pkg/nvmlutils"
	"gitlab.com/MaxIV/kubernetes/mortalgpu/pkg/sharecfg"
	"go.uber.org/zap"
	pluginapi "k8s.io/kubelet/pkg/apis/deviceplugin/v1beta1"
)

// each meta gpu will start from 'mortalgpu-meta-[index-number]-[sequence-number]'
var reMetaDevicePrefix = regexp.MustCompile(`mortalgpu-meta-\d+-\d+-`)

func (m *NvidiaDeviceManager) CacheDevices(logger *zap.Logger) {
	logger = logger.With(zap.String("thread", "CacheDevices"))
	// enforce device discovery
	// to make sure all the devices will be set
	// before kubelet api server will be started
	m.setDevices(logger)
	go func() {
		for {
			<-time.After(m.cacheTTL)
			m.setDevices(logger)
		}
	}()
}

func (m *NvidiaDeviceManager) ParseRealDeviceId(logger *zap.Logger, metaDevicesIds []string) (realDevicesIds []string) {
	logger = logger.Named("ParseRealDeviceId")

	// string map will eliminate doubles in real Devices ids
	realDevicesIdsMap := make(map[string]bool, len(metaDevicesIds))

	m.mtx.RLock()

	for _, metaDeviceId := range metaDevicesIds {
		deviceId := reMetaDevicePrefix.ReplaceAllString(metaDeviceId, "")
		if !m.DeviceExists(deviceId) {
			logger.With(zap.String("device", metaDeviceId)).Error("device does not exists, but was claimed")
			continue
		}
		realDevicesIdsMap[deviceId] = true
	}

	m.mtx.RUnlock()

	var realDevicesIdsList []string
	for dId := range realDevicesIdsMap {
		realDevicesIdsList = append(realDevicesIdsList, dId)
	}

	if l := logger.Check(zap.DebugLevel, "real devices id list"); l != nil {
		l.Write(zap.Strings("real_devices_ids", realDevicesIds))
	}

	return realDevicesIdsList
}

func (m *NvidiaDeviceManager) DeviceExists(deviceId string) bool {
	m.mtx.RLock()
	defer m.mtx.RUnlock()

	for _, d := range m.devices {
		if d.UUID == deviceId {
			return true
		}
	}
	return false
}

func (m *NvidiaDeviceManager) GetPluginDevices(logger *zap.Logger) []*pluginapi.Device {
	var metaGpus []*pluginapi.Device

	m.mtx.RLock()
	defer m.mtx.RUnlock()

	logger.With(zap.Int("total", len(m.devices)*m.shareCfg.MetagpusPerGpu)).Info("generating meta gpu devices")

	for _, d := range m.devices {
		for j := 0; j < m.shareCfg.MetagpusPerGpu; j++ {
			metaGpus = append(metaGpus, &pluginapi.Device{
				ID:     fmt.Sprintf("mortalgpu-meta-%d-%d-%s", d.Index, j, d.UUID),
				Health: pluginapi.Healthy,
			})
		}
	}

	return metaGpus
}

func (m *NvidiaDeviceManager) setDevices(logger *zap.Logger) {
	logger = logger.Named("setDevices")

	var devices []*MetaDevice
	nvmlDevices := nvmlutils.GetDevices(logger)
	logger.With(zap.Int("total", len(nvmlDevices))).Info("refreshing nvidia devices cache")

	var idx int = 0
	for _, device := range nvmlDevices {
		uuid := nvmlutils.GetDeviceUUID(logger, device)
		if nvmlutils.IsMigDevice(logger, device) {
			for _, migdevice := range nvmlutils.GetMigDevices(logger, device) {
				miguuid := nvmlutils.GetDeviceUUID(logger, migdevice)
				migidx := nvmlutils.GetMigInstanceId(logger, migdevice)
				if m.isManagedDevice(uuid, migidx) {
					// "On MIG-enabled GPUs, accounting mode would be set to DISABLED and changing it is not supported."
					// TODO: why we need an accounting here? It seams this is historical stats about processes that already finished
					devices = append(devices, &MetaDevice{
						UUID:  miguuid,
						Index: idx,
					})
					idx++
				}
			}
		} else {
			if m.isManagedDevice(uuid, -1) {
				// enable accounting mode
				nvmlutils.EnableAccountingMode(logger, device)
				// verify accounting mode is on
				state := nvmlutils.GetAccountingMode(logger, device)
				logger.With(zap.String("device", uuid)).With(zap.Int32("state", int32(state))).Info("accounting mode for device")
				devices = append(devices, &MetaDevice{UUID: uuid, Index: idx})
				idx++
			}
		}
	}

	m.mtx.Lock()
	m.devices = devices
	m.mtx.Unlock()
}

func (m *NvidiaDeviceManager) isManagedDevice(deviceUuid string, migId int) bool {
	m.mtx.RLock()
	defer m.mtx.RUnlock()

	var devmanaged bool = false
	if len(m.shareCfg.Uuid) == 0 {
		devmanaged = true
	} else {
		for _, uuid := range m.shareCfg.Uuid {
			if uuid == deviceUuid {
				devmanaged = true
				break
			}
		}
	}
	if devmanaged {
		if migId < 0 || len(m.shareCfg.Migid) == 0 {
			return true
		}
		for _, iid := range m.shareCfg.Migid {
			if migId == iid {
				return true
			}
		}
	}
	return false
}

func (m *NvidiaDeviceManager) GetDeviceSharingConfig() *sharecfg.DeviceSharingConfig {
	return m.shareCfg
}

func (m *NvidiaDeviceManager) GetUnixSocket() string {
	return b64.StdEncoding.EncodeToString([]byte(m.shareCfg.ResourceName))
}

func (m *NvidiaDeviceManager) MetagpuAllocation(logger *zap.Logger, allocationSize int, availableDevIds []string) ([]string, error) {
	logger = logger.Named("MetagpuAllocation")

	allocation, err := allocator.NewDeviceAllocation(
		logger,
		len(m.devices),
		allocationSize,
		m.shareCfg.MetagpusPerGpu,
		availableDevIds)
	if err != nil {
		return nil, fmt.Errorf("MetagpuAllocation: %w", err)
	}

	return allocation.GetMetagpusAllocations(), nil
}

func NewNvidiaDeviceManager(logger *zap.Logger, shareCfg *sharecfg.DeviceSharingConfig) *NvidiaDeviceManager {
	logger = logger.Named("NvidiaDeviceManager")

	ndm := &NvidiaDeviceManager{
		cacheTTL: time.Second * time.Duration(viper.GetInt("deviceCacheTTL")),
		shareCfg: shareCfg,
	}
	// start devices cache loop
	ndm.CacheDevices(logger)
	return ndm
}
