package plugin

import (
	"context"
	"fmt"
	"net"
	"os"
	"path"
	"path/filepath"
	"strings"
	"time"

	"golang.org/x/exp/slices"

	"github.com/spf13/viper"
	"go.uber.org/zap"
	"google.golang.org/grpc"
	pluginapi "k8s.io/kubelet/pkg/apis/deviceplugin/v1beta1"
)

const (
	kubeletSocket              = pluginapi.KubeletSocket
	kubeletSocketWatchInterval = 5 * time.Second
)

func (p *MetaGpuDevicePlugin) dial(socket string, timeout time.Duration) (*grpc.ClientConn, error) {
	c, err := grpc.Dial(socket, grpc.WithInsecure(), grpc.WithBlock(),
		grpc.WithContextDialer(func(ctx context.Context, s string) (net.Conn, error) {
			return net.DialTimeout("unix", socket, timeout)
		}),
	)

	if err != nil {
		return nil, err
	}

	return c, nil

}

func (p *MetaGpuDevicePlugin) Register(logger *zap.Logger) error {
	logger = logger.Named("gRPC_method").Named("Register")

	conn, err := p.dial(kubeletSocket, 5*time.Second)
	if err != nil {
		logger.With(zap.Error(err)).Error("can not connect to Kubelet")

		return err
	}
	defer conn.Close()
	client := pluginapi.NewRegistrationClient(conn)
	req := &pluginapi.RegisterRequest{
		Version:      pluginapi.Version,
		Endpoint:     path.Base(p.socket),
		ResourceName: p.GetDeviceSharingConfig().ResourceName,
		Options: &pluginapi.DevicePluginOptions{
			GetPreferredAllocationAvailable: true,
		},
	}
	if _, err := client.Register(context.Background(), req); err != nil {
		logger.With(zap.Error(err)).Error("can not register")

		return err
	}
	return nil
}

func (p *MetaGpuDevicePlugin) GetDevicePluginOptions(ctx context.Context, empty *pluginapi.Empty) (*pluginapi.DevicePluginOptions, error) {
	return &pluginapi.DevicePluginOptions{GetPreferredAllocationAvailable: true}, nil
}

func (p *MetaGpuDevicePlugin) ListAndWatch(e *pluginapi.Empty, s pluginapi.DevicePlugin_ListAndWatchServer) error {
	var logger = p.logger.Named("gRPC_method").Named("ListAndWatch")

	devices := p.GetPluginDevices(logger)

	if l := logger.Check(zap.DebugLevel, "GetPluginDevices result"); l != nil {
		l.Write(zap.Any("devices", devices))
	}

	if err := s.Send(&pluginapi.ListAndWatchResponse{Devices: devices}); err != nil {
		logger.Error("can not connect to DevicePlugin_ListAndWatchServer", zap.Error(err))
	}

	for {
		select {
		case <-p.ctx.Done():
			return nil
		case <-p.metaGpuRecalculation:
			devices := p.GetPluginDevices(logger)

			if l := logger.Check(zap.DebugLevel, "GetPluginDevices result"); l != nil {
				l.Write(zap.Any("devices", devices))
			}

			if err := s.Send(&pluginapi.ListAndWatchResponse{Devices: devices}); err != nil {
				logger.Error("can not connect to DevicePlugin_ListAndWatchServer", zap.Error(err))
			}
		}
	}
}

func (p *MetaGpuDevicePlugin) GetPreferredAllocation(ctx context.Context, request *pluginapi.PreferredAllocationRequest) (*pluginapi.PreferredAllocationResponse, error) {
	var plLogger = p.logger.Named("gRPC_method").Named("GetPreferredAllocation")

	allocResponse := &pluginapi.PreferredAllocationResponse{}
	for _, req := range request.ContainerRequests {
		// Annotate logger with available fields.
		var logger = plLogger.With(zap.Int32("req_allocation_size", req.GetAllocationSize()))

		if logger.Level().Enabled(zap.DebugLevel) {
			logger = logger.
				With(zap.Strings("req_available_device_ids", req.GetAvailableDeviceIDs())).
				With(zap.Strings("req_must_include_device_ids", req.GetMustIncludeDeviceIDs()))
		}

		allocContainerResponse := &pluginapi.ContainerPreferredAllocationResponse{}
		allocContainerResponse.DeviceIDs, _ = p.MetagpuAllocation(logger, int(req.AllocationSize), req.GetAvailableDeviceIDs())

		if logger.Level().Enabled(zap.DebugLevel) {
			logger.Debug("preferred devices ids:", zap.Any("device_ids", allocContainerResponse.DeviceIDs))
		}

		allocResponse.ContainerResponses = append(allocResponse.ContainerResponses, allocContainerResponse)
	}
	return allocResponse, nil
}

func (p *MetaGpuDevicePlugin) Allocate(ctx context.Context, request *pluginapi.AllocateRequest) (*pluginapi.AllocateResponse, error) {
	var pLogger = p.logger.Named("gRPC_method").Named("Allocate")

	allocResponse := &pluginapi.AllocateResponse{}
	for _, req := range request.ContainerRequests {
		var logger = pLogger

		if logger.Level().Enabled(zap.DebugLevel) {
			logger = logger.With(zap.Strings("requested_device_ids", req.GetDevicesIDs()))
		}

		response := pluginapi.ContainerAllocateResponse{}
		// set GPU container env variables
		slices.Sort(req.DevicesIDs)

		logger.Info("requested devices ids:", zap.Strings("devices_ids", req.DevicesIDs))

		realDevices := p.ParseRealDeviceId(logger, req.DevicesIDs)
		response.Envs = map[string]string{
			"NVIDIA_VISIBLE_DEVICES": strings.Join(realDevices, ","),
			"MG_CTL_ADDR":            fmt.Sprintf("%s:50052", os.Getenv("POD_IP")),
			"MG_CTL_TOKEN":           viper.GetString("containerToken"),
		}

		// use DevicePlugin hostpath mount to provide mgctl to container
		var mounts []*pluginapi.Mount
		if viper.GetBool("mgctlMount") {
			mgctlHostPath := viper.GetString("mgctlMountHostPath")
			if mgctlHostPath == "" {
				mgctlHostPath = "/var/lib/metagpu"
			}
			mgctlContainertPath := viper.GetString("mgctlMountContainertPath")
			if mgctlContainertPath == "" {
				mgctlContainertPath = "/usr/bin"
			}
			mount := &pluginapi.Mount{
				HostPath:      filepath.Join(mgctlHostPath, "mgctl"),
				ContainerPath: filepath.Join(mgctlContainertPath, "mgctl"),
				ReadOnly:      true,
			}
			mounts = append(mounts, mount)
		}
		response.Mounts = mounts
		allocResponse.ContainerResponses = append(allocResponse.ContainerResponses, &response)
	}

	if l := pLogger.Check(zap.DebugLevel, "Admission response"); l != nil {
		l.Write(zap.Any("response", allocResponse))
	}

	return allocResponse, nil
}

func (p *MetaGpuDevicePlugin) PreStartContainer(ctx context.Context, request *pluginapi.PreStartContainerRequest) (*pluginapi.PreStartContainerResponse, error) {
	return &pluginapi.PreStartContainerResponse{}, nil
}

func (p *MetaGpuDevicePlugin) Serve(logger *zap.Logger) error {
	logger = logger.Named("Serve")

	// Start gRPC socket server.
	go func() {
		logger := logger.Named("gRPCServer")

		_ = os.Remove(p.socket)

		sock, err := net.Listen("unix", p.socket)
		if err != nil {
			logger.With(zap.Error(err)).Error("can not listen to GRPC socket")
		}
		defer sock.Close()

		logger.With(zap.String("socket", p.socket)).Info("gRCP socket listening")

		logger.Info("Initializing gRPC server")
		p.server = grpc.NewServer([]grpc.ServerOption{}...)

		pluginapi.RegisterDevicePluginServer(p.server, p)

		if err := p.server.Serve(sock); err != nil {
			logger.With(zap.Error(err)).Error("gRPC server crashed")
		}
	}()

	if conn, err := p.dial(p.socket, 3*time.Second); err != nil {
		logger.With(zap.Error(err)).Error("GRPC server socket connection test failed")
		return err
	} else {
		_ = conn.Close()
		logger.Info("gRPC server successfully started and ready accept new connections")
	}

	return nil
}

func (p *MetaGpuDevicePlugin) Start() {
	var logger = p.logger.Named("Start")

	p.ctx, p.ctxCf = context.WithCancel(context.Background())

	if err := p.Serve(logger); err != nil {
		logger.Fatal("GRPC server failed", zap.Error(err))
	}

	if err := p.Register(logger); err != nil {
		logger.Fatal("Plugin registration failed", zap.Error(err))
	}

	p.StartWatchKubelet(logger)
}

func (p *MetaGpuDevicePlugin) Stop() {
	var logger *zap.Logger

	if p == nil {
		logger = zap.L().Named("MetaGpuDevicePlugin") // Fallback to default logger.
	} else {
		logger = p.logger
	}

	logger = logger.Named("Stop")

	logger.Info("stopping GRPC server")
	if p != nil && p.server != nil {
		p.server.Stop()
	}
	logger.Info("removing unix socket")
	_ = os.Remove(p.socket)
	logger.Info("closing all channels")
	p.ctxCf()
}

func (p *MetaGpuDevicePlugin) StartWatchKubelet(logger *zap.Logger) {
	// Inspired by https://github.com/Mellanox/k8s-rdma-shared-dev-plugin/blob/master/pkg/resources/server.go#L224.
	go func() {
		logger = logger.Named("StartWatchKubelet")

		logger.Info("Starting watching Kubelet socket to reach on Kubelet restarts")

		st, err := os.Lstat(kubeletSocket)
		if err != nil {
			logger.Fatal("Kubelet socket is not found", zap.Error(err), zap.String("socket_path", kubeletSocket))
		}

		logger.Info("Kubelet socket is present",
			zap.String("socket_path", kubeletSocket), zap.Time("kubelet_socket_mod_time", st.ModTime()))

		var initKubeletSocketTS = st.ModTime()

		tk := time.NewTicker(time.Second * 5)

		for {
			select {
			case <-p.ctx.Done():
				logger.Info("Stopping")
				tk.Stop()
				return

			case tm := <-tk.C:
				if l := logger.Check(zap.DebugLevel, "Checking Kubelet socket os.stat"); l != nil {
					l.Write(zap.Time("tick_ts", tm),
						zap.String("socket_path", kubeletSocket))
				}

				st, err := os.Lstat(kubeletSocket)
				if err != nil {
					logger.Warn("Kubelet socket is not found, most likely Kubelet was restarted",
						zap.Error(err), zap.String("socket_path", kubeletSocket))
					logger.Info("Waiting for 5 seconds and restarting MetaGPU device plugin")
					time.Sleep(5 * time.Second)
					logger.Info("Restarting MetaGPU device plugin")

					p.Stop()
					p.Start()

					return
				}

				if st.ModTime().Compare(initKubeletSocketTS) == 0 {
					if l := logger.Check(zap.DebugLevel, "Kubelet socket is present, modification time is not changed"); l != nil {
						l.Write(zap.String("socket_path", kubeletSocket), zap.Time("kubelet_socket_mod_time", st.ModTime()))
					}
				} else {
					logger.Warn("Kubelet socket is found but its modification time is changed. most likely Kubelet was restarted",
						zap.String("socket_path", kubeletSocket),
						zap.Time("old_mod_time", initKubeletSocketTS),
						zap.Time("new_mod_time", st.ModTime()))
					logger.Info("Waiting for 5 seconds and restarting MetaGPU device plugin")
					time.Sleep(5 * time.Second)
					logger.Info("Restarting MetaGPU device plugin")

					p.Stop()
					p.Start()

					return
				}

			}
		}
	}()
}

func NewMetaGpuDevicePlugin(logger *zap.Logger, metaGpuRecalculation chan bool, deviceMgr DeviceManager) *MetaGpuDevicePlugin {
	var log = logger.Named("MetaGpuDevicePlugin")

	if viper.GetString("accelerator") != "nvidia" {
		log.Fatal("accelerator not supported, currently only nvidia is supported")
	}

	return &MetaGpuDevicePlugin{
		server:               nil,
		socket:               fmt.Sprintf("%s%s", pluginapi.DevicePluginPath, deviceMgr.GetUnixSocket()),
		DeviceManager:        deviceMgr,
		metaGpuRecalculation: metaGpuRecalculation,
		logger:               log,
	}
}
