package v1

import (
	"context"
	"time"

	"go.uber.org/zap"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	pb "gitlab.com/MaxIV/kubernetes/mortalgpu/gen/proto/go/device/v1"
	"gitlab.com/MaxIV/kubernetes/mortalgpu/pkg/gpumgr"
)

type DeviceService struct {
	pb.UnimplementedDeviceServiceServer
	gpuMgr *gpumgr.GpuMgr
	vl     string // visibility level
	cvl    string // container visibility level ID
	dvl    string // device visibility level ID

	logger *zap.Logger
}

func (s *DeviceService) LoadContext(ctx context.Context) error {
	logger, ok := ctx.Value("logger").(*zap.Logger)
	if !ok {
		zap.L().Fatal("Zap logger is not set in context")
	}
	s.logger = logger.Named("DeviceService")

	s.gpuMgr = ctx.Value("gpuMgr").(*gpumgr.GpuMgr)
	if s.gpuMgr == nil {
		s.logger.Fatal("gpuMgr instance not set in context")
	}
	s.vl = ctx.Value("visibilityLevel").(string)
	s.cvl = ctx.Value("containerVl").(string)
	s.dvl = ctx.Value("deviceVl").(string)

	// stop execution if visibility level is empty
	if s.vl == "" {
		return status.Errorf(codes.Aborted, "can't detect visibility level for request: %s", s.vl)
	}
	// stop executing if container or device visibility level is empty
	if s.cvl == "" || s.dvl == "" {
		return status.Error(codes.Aborted, "can't detect visibility levels")
	}
	return nil
}

func (s *DeviceService) GetGpuContainers(ctx context.Context, r *pb.GetGpuContainersRequest) (*pb.GetGpuContainersResponse, error) {
	if err := s.LoadContext(ctx); err != nil {
		return &pb.GetGpuContainersResponse{}, err
	}
	response := &pb.GetGpuContainersResponse{VisibilityLevel: s.vl}
	// stop execution if visibility level is container and pod id is not set (not enough permissions)
	if s.vl == s.cvl && r.GetPodId() == "" {
		return response, status.Errorf(codes.PermissionDenied, "missing pod id and visibility level is to low (%s), can't proceed", s.vl)
	}
	if s.vl == s.dvl {
		r.PodId = "" // for deviceVisibilityLevel server should return all running process on all containers
	}
	response.GpuContainers = listDeviceProcesses(r.GetPodId(), s.gpuMgr)
	return response, nil
}

func (s *DeviceService) StreamGpuContainers(r *pb.StreamGpuContainersRequest, stream pb.DeviceService_StreamGpuContainersServer) error {
	for {
		if err := s.LoadContext(stream.Context()); err != nil {
			return err
		}
		// stop execution if visibility level is container and pod id is not set (not enough permissions)
		if s.vl == s.cvl && r.GetPodId() == "" {
			return status.Errorf(codes.PermissionDenied, "missing pod id and visibility level is to low (%s), can't proceed", s.vl)
		}
		if s.vl == s.dvl {
			r.PodId = "" // for deviceVisibilityLevel server should return all running process on all containers
		}
		response := &pb.StreamGpuContainersResponse{VisibilityLevel: s.vl}
		response.GpuContainers = listDeviceProcesses(r.GetPodId(), s.gpuMgr)
		if err := stream.Send(response); err != nil {
			return err
		}

		time.Sleep(1 * time.Second)
	}
}

func (s *DeviceService) GetDevices(ctx context.Context, r *pb.GetDevicesRequest) (*pb.GetDevicesResponse, error) {
	response := &pb.GetDevicesResponse{}
	if err := s.LoadContext(ctx); err != nil {
		return response, err
	}

	response.Device = make(map[string]*pb.Device)

	for _, device := range s.gpuMgr.GetMetaDevices() {
		var (
			utilization = device.GetUtilization()
			memory      = device.GetMemory()
		)

		d := &pb.Device{
			Uuid:              device.GetUUID(),
			Index:             uint32(device.GetDeviceIndex()),
			AllocatedShares:   uint32(device.GetAllocatedShares()),
			Shares:            uint32(device.GetShares()),
			GpuUtilization:    utilization.Gpu,
			MemoryUtilization: utilization.Memory,
			MemoryShareSize:   memory.ShareSize,
			ResourceName:      device.GetResourceName(),
			NodeName:          device.GetNodeName(),
		}
		if s.vl == s.dvl {
			d.MemoryTotal = memory.Total
			d.MemoryFree = memory.Free
			d.MemoryUsed = memory.Used
		}

		response.Device[d.GetUuid()] = d
	}
	return response, nil
}

func (s *DeviceService) KillGpuProcess(ctx context.Context, r *pb.KillGpuProcessRequest) (*pb.KillGpuProcessResponse, error) {
	response := &pb.KillGpuProcessResponse{}
	if err := s.LoadContext(ctx); err != nil {
		return response, err
	}
	if err := s.gpuMgr.KillGpuProcess(r.GetPid()); err != nil {
		return response, status.Errorf(codes.Internal, "error killing GPU process, err: %s", err)
	}
	return response, nil
}

func (s *DeviceService) GetMetaDeviceInfo(ctx context.Context, r *pb.GetMetaDeviceInfoRequest) (*pb.GetMetaDeviceInfoResponse, error) {
	resp := &pb.GetMetaDeviceInfoResponse{}
	if err := s.LoadContext(ctx); err != nil {
		return resp, err
	}
	if s.vl != s.dvl {
		return resp, status.Errorf(codes.PermissionDenied, "wrong visibility level: %s", s.vl)
	}
	deviceInfo := s.gpuMgr.GetDeviceInfo()
	resp.Node = deviceInfo.Node
	resp.Metadata = deviceInfo.Metadata
	for _, device := range deviceInfo.Devices {
		var (
			utilization = device.GetUtilization()
			memory      = device.GetMemory()
		)

		resp.Devices = append(resp.GetDevices(), &pb.Device{
			Uuid:              device.GetUUID(),
			Index:             uint32(device.GetDeviceIndex()),
			AllocatedShares:   uint32(device.GetAllocatedShares()),
			Shares:            uint32(device.GetShares()),
			GpuUtilization:    utilization.Gpu,
			MemoryUtilization: utilization.Memory,
			MemoryShareSize:   memory.ShareSize,
			MemoryTotal:       memory.Total,
			MemoryFree:        memory.Free,
			MemoryUsed:        memory.Used,
			ResourceName:      device.GetResourceName(),
			NodeName:          device.GetNodeName(),
		})
	}
	return resp, nil
}

func (s *DeviceService) PatchConfigs(ctx context.Context, r *pb.PatchConfigsRequest) (*pb.PatchConfigsResponse, error) {
	// if err := s.LoadContext(ctx); err != nil {
	//	return &pb.PatchConfigsResponse{}, err
	//}
	//if s.vl != s.dvl {
	//	return &pb.PatchConfigsResponse{}, status.Errorf(codes.PermissionDenied, "visibility level too high", s.vl)
	//}
	//deviceplugin.UpdatePersistentConfigs(r.MetagpusPerGpu)
	//viper.Set("metaGpus", r.MetagpusPerGpu)
	//s.gpuMgr.MetaGpuRecalculation <- true
	return &pb.PatchConfigsResponse{}, nil
}

func (s *DeviceService) PingServer(ctx context.Context, r *pb.PingServerRequest) (*pb.PingServerResponse, error) {
	return &pb.PingServerResponse{}, nil
}
