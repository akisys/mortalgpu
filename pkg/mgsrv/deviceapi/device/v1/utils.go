package v1

import (
	pb "gitlab.com/MaxIV/kubernetes/mortalgpu/gen/proto/go/device/v1"
	"gitlab.com/MaxIV/kubernetes/mortalgpu/pkg/gpumgr"
)

func listDeviceProcesses(podID string, gpuMgr *gpumgr.GpuMgr) (containers []*pb.GpuContainer) {
	for _, _containers := range gpuMgr.GetProcesses(podID) {
		for _, container := range _containers {
			var gpuProcesses []*pb.DeviceProcess

			for _, gpuProcess := range container.GetProcesses() {
				gpuProcesses = append(gpuProcesses, &pb.DeviceProcess{
					Mguuid:            gpuProcess.GetMetaGpuUUID(),
					Devuuid:           gpuProcess.GetDeviceUUID(),
					Pid:               gpuProcess.GetPid(),
					Memory:            gpuProcess.GetGPUMemory(),
					Cmdline:           gpuProcess.GetShortCmdLine(),
					User:              gpuProcess.GetUser(),
					ContainerId:       gpuProcess.GetContainerId(),
					GpuUtilization:    gpuProcess.GetGPUUtilization(),
					GpuInstanceId:     int64(gpuProcess.GetGPUInstanceId()),
					ComputeInstanceId: int64(gpuProcess.GetComputeInstanceId()),
					MigDevice:         -1,
				})
			}

			var gpuDevices []*pb.ContainerDevice

			for _, device := range container.GetDevices() {
				gpuDevice := device.GetGPUDevice()

				var (
					utilization = gpuDevice.GetUtilization()
					memory      = gpuDevice.GetMemory()
				)

				gpuDevices = append(gpuDevices, &pb.ContainerDevice{
					Device: &pb.Device{
						Uuid:              gpuDevice.GetUUID(),
						Devuuid:           gpuDevice.GetDeviceUUID(),
						Migid:             int64(gpuDevice.GetMigId()),
						Index:             uint32(gpuDevice.GetDeviceIndex()),
						AllocatedShares:   uint32(gpuDevice.GetAllocatedShares()),
						Shares:            uint32(gpuDevice.GetShares()),
						GpuUtilization:    utilization.Gpu,
						MemoryUtilization: utilization.Memory,
						MemoryTotal:       memory.Total,
						MemoryFree:        memory.Free,
						MemoryUsed:        memory.Used,
						MemoryShareSize:   memory.ShareSize,
						ResourceName:      gpuDevice.GetResourceName(),
						NodeName:          gpuDevice.GetNodeName(),
					},
					AllocatedShares: device.GetAllocatedShares(),
				})
			}

			containers = append(containers, &pb.GpuContainer{
				ContainerId:      container.GetContainerId(),
				ContainerName:    container.GetContainerName(),
				PodId:            container.GetPodId(),
				PodNamespace:     container.GetPodNamespace(),
				MetagpuRequests:  container.GetPodMetaGPURequest(),
				MetagpuLimits:    container.GetPodMetaGPULimit(),
				ResourceName:     container.GetResourceName(),
				NodeName:         container.GetNodeName(),
				ContainerDevices: gpuDevices,
				DeviceProcesses:  gpuProcesses,
			})
		}
	}

	return containers
}
