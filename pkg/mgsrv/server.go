package mgsrv

import (
	"context"
	"errors"
	"net"

	"github.com/golang-jwt/jwt"
	"github.com/spf13/viper"
	devicevpb "gitlab.com/MaxIV/kubernetes/mortalgpu/gen/proto/go/device/v1"
	"gitlab.com/MaxIV/kubernetes/mortalgpu/pkg/gpumgr"
	devicevapi "gitlab.com/MaxIV/kubernetes/mortalgpu/pkg/mgsrv/deviceapi/device/v1"
	"go.uber.org/zap"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/reflection"
	"google.golang.org/grpc/status"
)

type VisibilityLevel string

type MetaGpuServer struct {
	gpuMgr                        *gpumgr.GpuMgr
	ContainerLevelVisibilityToken string
	DeviceLevelVisibilityToken    string

	logger *zap.Logger
}

var (
	DeviceVisibility         VisibilityLevel = "l0"
	ContainerVisibility      VisibilityLevel = "l1"
	TokenVisibilityClaimName                 = "visibilityLevel"
)

func NewMetaGpuServer(logger *zap.Logger) *MetaGpuServer {
	logger = logger.Named("MetaGpuServer")

	return &MetaGpuServer{gpuMgr: gpumgr.NewGpuManager(logger), logger: logger}
}

func (s *MetaGpuServer) Start() {
	var logger = s.logger.Named("Start")

	go func() {
		lis, err := net.Listen("tcp", viper.GetString("serverAddr"))
		if err != nil {
			logger.Fatal("failed to listen", zap.Error(err))
		}
		logger.Info("metagpu gRPC management server listening", zap.String("address", viper.GetString("serverAddr")))

		opts := []grpc.ServerOption{
			grpc.UnaryInterceptor(s.unaryServerInterceptor()),
			grpc.StreamInterceptor(s.streamServerInterceptor()),
		}

		grpcServer := grpc.NewServer(opts...)

		dp := devicevapi.DeviceService{}
		devicevpb.RegisterDeviceServiceServer(grpcServer, &dp)
		reflection.Register(grpcServer)
		if err := grpcServer.Serve(lis); err != nil {
			logger.Fatal("gRPC server can not start", zap.Error(err))
		}
	}()
}

func (s *MetaGpuServer) GenerateAuthTokens(visibility VisibilityLevel) string {

	claims := jwt.MapClaims{"email": "metagpu@instance", TokenVisibilityClaimName: visibility}
	containerScopeToken := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	tokenString, err := containerScopeToken.SignedString([]byte(viper.GetString("jwtSecret")))
	if err != nil {
		s.logger.Named("GenerateAuthTokens").Error("can not generate container scope token", zap.Error(err))
	}
	return tokenString
}

func (s *MetaGpuServer) IsMethodPublic(fullMethod string) bool {
	publicMethods := []string{
		"/device.v1.DeviceService/PingServer",
	}
	for _, method := range publicMethods {
		if method == fullMethod {
			return true
		}
	}
	return false

}

func authorize(ctx context.Context, logger *zap.Logger) (string, error) {
	logger = logger.Named("authorize")

	md, ok := metadata.FromIncomingContext(ctx)
	if !ok {
		return "", status.Errorf(codes.InvalidArgument, "retrieving metadata is failed")
	}

	authHeader, ok := md["authorization"]

	if !ok {
		return "", status.Errorf(codes.Unauthenticated, "authorization token is not supplied")
	}

	tokenString := authHeader[0]
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			logger.Named("authorize").Error("unexpected signing method", zap.Any("method", token.Header["alg"]))
			return nil, status.Errorf(codes.Unauthenticated, errors.New("error authenticate").Error())
		}
		return []byte(viper.GetString("jwtSecret")), nil
	})
	if err != nil {
		logger.Named("authorize").Error("can not authorize", zap.Error(err))

		return "", status.Errorf(codes.Unauthenticated, errors.New("error authenticate").Error())
	}

	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		if visibility, ok := claims[TokenVisibilityClaimName]; ok {
			visibility := visibility.(string)
			return visibility, nil
		}
	}
	return "", status.Errorf(codes.Unauthenticated, errors.New("error authenticate").Error())
}
