package mgsrv

import (
	"context"
	"time"

	"go.uber.org/zap"
	"google.golang.org/grpc"
)

type MetaGpuServerStream struct {
	grpc.ServerStream
	ctx context.Context
}

func (s *MetaGpuServerStream) Context() context.Context {
	return s.ctx
}

func (s *MetaGpuServer) streamServerInterceptor() grpc.StreamServerInterceptor {
	var logger = s.logger.Named("streamServerInterceptor")

	return func(srv interface{}, ss grpc.ServerStream, info *grpc.StreamServerInfo, handler grpc.StreamHandler) error {
		wrapper := &MetaGpuServerStream{ServerStream: ss}
		if !s.IsMethodPublic(info.FullMethod) {
			visibility, err := authorize(ss.Context(), logger)
			if err != nil {
				return err
			}
			wrapper.ctx = context.WithValue(ss.Context(), TokenVisibilityClaimName, visibility)
			wrapper.ctx = context.WithValue(wrapper.ctx, "containerVl", string(ContainerVisibility))
			wrapper.ctx = context.WithValue(wrapper.ctx, "deviceVl", string(DeviceVisibility))
			wrapper.ctx = context.WithValue(wrapper.ctx, "gpuMgr", s.gpuMgr)
			wrapper.ctx = context.WithValue(wrapper.ctx, "logger", logger)

		}
		return handler(srv, wrapper)
	}
}

func (s *MetaGpuServer) unaryServerInterceptor() grpc.UnaryServerInterceptor {
	var logger = s.logger.Named("unaryServerInterceptor")

	return func(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (interface{}, error) {
		start := time.Now()

		if !s.IsMethodPublic(info.FullMethod) {
			visibility, err := authorize(ctx, logger)
			if err != nil {
				return nil, err
			}
			ctx = context.WithValue(ctx, TokenVisibilityClaimName, visibility)
			ctx = context.WithValue(ctx, "containerVl", string(ContainerVisibility))
			ctx = context.WithValue(ctx, "deviceVl", string(DeviceVisibility))
			ctx = context.WithValue(ctx, "logger", logger)
		}
		ctx = context.WithValue(ctx, "gpuMgr", s.gpuMgr)
		h, err := handler(ctx, req)

		logger.Debug("Method performance", zap.String("method", info.FullMethod), zap.Duration("duration", time.Since(start)))

		return h, err
	}
}
