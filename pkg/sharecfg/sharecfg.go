package sharecfg

import (
	"fmt"

	"github.com/spf13/viper"
	"go.uber.org/zap"
)

type DeviceSharingConfig struct {
	Uuid           []string
	Migid          []int
	ResourceName   string
	MetagpusPerGpu int
}

type DevicesSharingConfigs struct {
	Configs []*DeviceSharingConfig

	logger *zap.Logger
}

var shareCfg *DevicesSharingConfigs

func NewDeviceSharingConfig(logger *zap.Logger) *DevicesSharingConfigs {
	if shareCfg != nil {
		return shareCfg
	}

	logger = logger.Named("DeviceSharingConfig")

	var cfg []*DeviceSharingConfig
	if err := viper.UnmarshalKey("deviceSharing", &cfg); err != nil {
		logger.Fatal("can not unmarshal deviceSharing configuration section", zap.Error(err))
	}
	shareCfg = &DevicesSharingConfigs{Configs: cfg, logger: logger}
	shareCfg.ValidateSharingConfiguration()
	return shareCfg
}

func (c *DevicesSharingConfigs) ValidateSharingConfiguration() {
	if len(c.Configs) == 0 {
		c.logger.Fatal("missing gpu sharing configuration, can't proceed")
	}
	if len(c.Configs) > 1 {
		for i, devCfg := range c.Configs {
			// TODO: check config sanity (resourceName regex)
			if devCfg.MetagpusPerGpu <= 0 {
				c.logger.Fatal("Wrong GPU sharing configuration: deviceSharing.metagpusPerGpu must be > 0.",
					zap.Int("device_sharing_cfg_index", i), zap.Int("device_sharing_configured_metagpus_per_gpu", devCfg.MetagpusPerGpu))
			}
		}
	}
}

func (c *DevicesSharingConfigs) GetDeviceSharingConfigs(devUuid string, migId int) (*DeviceSharingConfig, error) {
	for _, devCfg := range c.Configs {
		var devmatch bool = false
		if len(devCfg.Uuid) == 0 {
			devmatch = true
		} else {
			for _, uuid := range devCfg.Uuid {
				if uuid == devUuid {
					devmatch = true
					break
				}
			}
		}
		if devmatch {
			if migId < 0 || len(devCfg.Migid) == 0 {
				return devCfg, nil
			}
			for _, iid := range devCfg.Migid {
				if migId == iid {
					return devCfg, nil
				}
			}
		}
	}
	if migId < 0 {
		return nil, fmt.Errorf("device uuid: %s not found in sharing configs", devUuid)
	} else {
		return nil, fmt.Errorf("MIG instance %d on device uuid: %s not found in sharing configs", migId, devUuid)
	}
}
