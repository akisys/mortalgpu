package allocator

import "go.uber.org/zap"

// DeviceLoad - available MortalGPUs (GPU fractions).
type DeviceLoad struct {
	// metagpus - list of available (not allocated) GPU fractions.
	metagpus []string
}

type DeviceAllocation struct {
	loadMap []*DeviceLoad
	// allocationSize - number of devices to allocate (MortalGPU GPU fractions).
	allocationSize int
	// totalSharesPerGpu - number of meta GPUs per a physical GPU (or NVIDIA MIG).
	totalSharesPerGpu   int
	metagpusAllocations []string

	logger *zap.Logger
}
