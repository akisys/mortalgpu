package allocator

import (
	"fmt"
	"testing"

	"go.uber.org/zap"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

func TestAllocator(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Allocation Suite")
}

var _ = Describe("Metagpu allocations", func() {
	Context("allocate", func() {
		logger, err := zap.NewDevelopment()
		if err != nil {
			panic(err)
		}

		It("10% gpu", func() {
			physDevs := 2
			allocationSize := 1
			sharesPerGpu := 10
			testDevices := getTestDevicesIds(physDevs, sharesPerGpu)
			alloc, err := NewDeviceAllocation(logger, physDevs, allocationSize, sharesPerGpu, testDevices)
			Expect(err).ToNot(HaveOccurred(), "Must not return error")
			Expect(len(alloc.metagpusAllocations)).To(Equal(1))
			expectedDevices := []string{"mortalgpu-meta-0-0-test-device-0"}
			Expect(alloc.metagpusAllocations).To(Equal(expectedDevices))
		})

		It("50% gpu", func() {
			physDevs := 2
			sharesPerGpu := 10
			allocationSize := 5
			testDevices := getTestDevicesIds(physDevs, sharesPerGpu)
			alloc, err := NewDeviceAllocation(logger, physDevs, allocationSize, sharesPerGpu, testDevices)
			Expect(err).ToNot(HaveOccurred(), "Must not return error")
			Expect(len(alloc.metagpusAllocations)).To(Equal(5))
			Expect(alloc.metagpusAllocations).To(Equal(getTestDevicesIds(1, 5)))
		})

		It("80% gpu", func() {
			physDevs := 2
			sharesPerGpu := 10
			allocationSize := 8
			testDevices := getTestDevicesIds(physDevs, sharesPerGpu)
			alloc, err := NewDeviceAllocation(logger, physDevs, allocationSize, sharesPerGpu, testDevices)
			Expect(err).ToNot(HaveOccurred(), "Must not return error")
			Expect(len(alloc.metagpusAllocations)).To(Equal(8))
			Expect(alloc.metagpusAllocations).To(Equal(getTestDevicesIds(1, 8)))
		})

		It("100% gpu", func() {
			physDevs := 2
			allocationSize := 10
			sharesPerGpu := 10
			testDevices := getTestDevicesIds(physDevs, sharesPerGpu)
			alloc, err := NewDeviceAllocation(logger, physDevs, allocationSize, sharesPerGpu, testDevices)
			Expect(err).ToNot(HaveOccurred(), "Must not return error")
			Expect(len(alloc.metagpusAllocations)).To(Equal(10))
			Expect(alloc.metagpusAllocations).To(Equal(getTestDevicesIds(1, 10)))
		})

		It("110% gpu", func() {
			physDevs := 2
			allocationSize := 12
			sharesPerGpu := 10
			testDevices := getTestDevicesIds(physDevs, sharesPerGpu)
			alloc, err := NewDeviceAllocation(logger, physDevs, allocationSize, sharesPerGpu, testDevices)
			Expect(err).ToNot(HaveOccurred(), "Must not return error")
			Expect(len(alloc.metagpusAllocations)).To(Equal(12))
			expectedIds := getTestDevicesIds(1, 10)
			expectedIds = append(expectedIds, "mortalgpu-meta-1-0-test-device-1")
			expectedIds = append(expectedIds, "mortalgpu-meta-1-1-test-device-1")
			Expect(alloc.metagpusAllocations).To(Equal(expectedIds))
		})

		It("200% gpu", func() {
			physDevs := 2
			allocationSize := 20
			sharesPerGpu := 10
			testDevices := getTestDevicesIds(physDevs, sharesPerGpu)
			alloc, err := NewDeviceAllocation(logger, physDevs, allocationSize, sharesPerGpu, testDevices)
			Expect(err).ToNot(HaveOccurred(), "Must not return error")
			Expect(len(alloc.metagpusAllocations)).To(Equal(20))
			Expect(alloc.metagpusAllocations).To(Equal(getTestDevicesIds(2, 10)))
		})

		It("single GPU -> 50% after 50% has been taken", func() {
			devices := []string{
				// "mortalgpu-meta-0-0-test-device-0", -> already allocated by previous request
				// "mortalgpu-meta-0-1-test-device-0",  -> already allocated by previous request
				"mortalgpu-meta-0-2-test-device-0", // -> this should be allocated now
				"mortalgpu-meta-0-3-test-device-0", // -> this should be allocated now
				"mortalgpu-meta-1-0-test-device-1",
				"mortalgpu-meta-1-1-test-device-1",
				"mortalgpu-meta-1-2-test-device-1",
				"mortalgpu-meta-1-3-test-device-1",
			}
			physDevs := 2
			allocationSize := 2
			sharesPerGpu := 4
			alloc, err := NewDeviceAllocation(logger, physDevs, allocationSize, sharesPerGpu, devices)
			Expect(err).ToNot(HaveOccurred(), "Must not return error")
			Expect(len(alloc.metagpusAllocations)).To(Equal(2))
			Expect(alloc.metagpusAllocations).To(Equal([]string{"mortalgpu-meta-0-2-test-device-0", "mortalgpu-meta-0-3-test-device-0"}))
		})

		It("single GPU -> 60% after 50% has been taken (jump to next device)", func() {
			devices := []string{
				// "mortalgpu-meta-0-0-test-device-0", -> already allocated by previous request
				// "mortalgpu-meta-0-1-test-device-0",  -> already allocated by previous request
				"mortalgpu-meta-0-2-test-device-0",
				"mortalgpu-meta-0-3-test-device-0",
				"mortalgpu-meta-1-0-test-device-1", // -> this should be allocated now
				"mortalgpu-meta-1-1-test-device-1", // -> this should be allocated now
				"mortalgpu-meta-1-2-test-device-1", // -> this should be allocated now
				"mortalgpu-meta-1-3-test-device-1",
			}
			physDevs := 2
			allocationSize := 3
			sharesPerGpu := 4
			alloc, err := NewDeviceAllocation(logger, physDevs, allocationSize, sharesPerGpu, devices)
			Expect(err).ToNot(HaveOccurred(), "Must not return error")
			Expect(len(alloc.metagpusAllocations)).To(Equal(3))
			Expect(alloc.metagpusAllocations).To(Equal([]string{
				"mortalgpu-meta-1-0-test-device-1",
				"mortalgpu-meta-1-1-test-device-1",
				"mortalgpu-meta-1-2-test-device-1",
			}))
		})

		It("allocate fractions from 2 different physical gpus", func() {
			devices := []string{
				// "mortalgpu-meta-0-0-test-device-0", -> already allocated by previous request
				// "mortalgpu-meta-0-1-test-device-0", -> already allocated by previous request
				"mortalgpu-meta-0-2-test-device-0", // -> this should be allocated now
				"mortalgpu-meta-0-3-test-device-0", // -> this should be allocated now
				// "mortalgpu-meta-1-0-test-device-1", -> already allocated by previous request
				// "mortalgpu-meta-1-1-test-device-1", -> already allocated by previous request
				"mortalgpu-meta-1-2-test-device-1", // -> this should be allocated now
				"mortalgpu-meta-1-3-test-device-1",
			}
			physDevs := 2
			allocationSize := 3
			sharesPerGpu := 4
			alloc, err := NewDeviceAllocation(logger, physDevs, allocationSize, sharesPerGpu, devices)
			Expect(err).ToNot(HaveOccurred(), "Must not return error")
			Expect(len(alloc.metagpusAllocations)).To(Equal(3))
			Expect(alloc.metagpusAllocations).To(Equal([]string{
				"mortalgpu-meta-0-2-test-device-0",
				"mortalgpu-meta-0-3-test-device-0",
				"mortalgpu-meta-1-2-test-device-1",
			}))
		})
		It("allocate full from 2 different physical gpus", func() {
			devices := []string{
				// "mortalgpu-meta-0-0-test-device-0", -> already allocated by previous request
				// "mortalgpu-meta-0-1-test-device-0", -> already allocated by previous request
				"mortalgpu-meta-0-2-test-device-0", // -> this should be allocated now
				"mortalgpu-meta-0-3-test-device-0", // -> this should be allocated now
				// "mortalgpu-meta-1-0-test-device-1", -> already allocated by previous request
				// "mortalgpu-meta-1-1-test-device-1", -> already allocated by previous request
				"mortalgpu-meta-1-2-test-device-1", // -> this should be allocated now
				"mortalgpu-meta-1-3-test-device-1", // -> this should be allocated now
			}
			physDevs := 2
			allocationSize := 4
			sharesPerGpu := 4
			alloc, err := NewDeviceAllocation(logger, physDevs, allocationSize, sharesPerGpu, devices)
			Expect(err).ToNot(HaveOccurred(), "Must not return error")
			Expect(len(alloc.metagpusAllocations)).To(Equal(4))
			Expect(alloc.metagpusAllocations).To(Equal([]string{
				"mortalgpu-meta-0-2-test-device-0",
				"mortalgpu-meta-0-3-test-device-0",
				"mortalgpu-meta-1-2-test-device-1",
				"mortalgpu-meta-1-3-test-device-1",
			}))
		})
	})
})

func getTestDevicesIds(physicalDevices, sharesPerGpu int) (metagpus []string) {
	for i := 0; i < physicalDevices; i++ {
		for j := 0; j < sharesPerGpu; j++ {
			metagpus = append(metagpus, fmt.Sprintf("mortalgpu-meta-%d-%d-test-device-%d", i, j, i))
		}
	}
	return
}
