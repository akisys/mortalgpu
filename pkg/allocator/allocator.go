package allocator

import (
	"fmt"
	"regexp"
	"sort"
	"strconv"

	"go.uber.org/zap"
)

// each meta gpu will start from 'mortalgpu-meta-[index-number]-[sequence-number]'
var reMetaDeviceID = regexp.MustCompile(`mortalgpu-meta-(\d+)-\d+-`)

func NewDeviceAllocation(logger *zap.Logger, totalDevices, allocationSize, totalSharesPerGpu int, availableDevIds []string) (*DeviceAllocation, error) {
	sort.Strings(availableDevIds)
	devAlloc := &DeviceAllocation{
		allocationSize:    allocationSize,
		totalSharesPerGpu: totalSharesPerGpu,
		logger:            logger,
	}
	// print available device ids
	devAlloc.PrintAvailableDevIds(availableDevIds)

	// init load map
	devAlloc.loadMap = make([]*DeviceLoad, totalDevices)
	if err := devAlloc.initLoadMap(availableDevIds); err != nil {
		return nil, err
	}

	// make allocations
	devAlloc.allocate()

	return devAlloc, nil
}

func (a *DeviceAllocation) initLoadMap(availableDevIds []string) error {
	// build a map of real device id to meta device id
	for _, availableDevID := range availableDevIds {
		devIdx, err := metaDeviceIdToDeviceIndex(a.logger, availableDevID)
		if err != nil {
			return err
		}

		if a.loadMap[devIdx] == nil {
			a.loadMap[devIdx] = &DeviceLoad{metagpus: nil}
		}

		a.loadMap[devIdx].metagpus = append(a.loadMap[devIdx].metagpus, availableDevID)
	}

	return nil
}

func (a *DeviceAllocation) PrintAvailableDevIds(availableDevIds []string) {
	if a.logger.Level().Enabled(zap.DebugLevel) {
		a.logger.Named("PrintAvailableDevIds").Debug(
			"[preferred-allocation] available devices IDs",
			zap.Int("available_ids_cnt", len(availableDevIds)),
			zap.Any("device_ids", availableDevIds),
		)
	}
}

func (a *DeviceAllocation) GetMetagpusAllocations() []string {
	res := make([]string, len(a.metagpusAllocations))

	copy(res, a.metagpusAllocations)

	return res
}

func (a *DeviceAllocation) allocate() {
	entireGpusRequest := a.allocationSize / a.totalSharesPerGpu
	gpuFractionsRequest := a.allocationSize % a.totalSharesPerGpu

	var logger = a.logger.Named("allocate").
		With(zap.Int("full_gpus_request", entireGpusRequest)).
		With(zap.Int("fraction_gpu_request", gpuFractionsRequest))

	logger.Info("metagpu allocation request")

	// first try to allocate entire gpus if requested
	for i := 0; i < entireGpusRequest; i++ {
		for _, devLoad := range a.loadMap {
			if devLoad == nil {
				continue
			}
			if devLoad.getFreeShares() == a.totalSharesPerGpu {
				a.metagpusAllocations = append(a.metagpusAllocations, devLoad.metagpus...)

				if l := logger.Check(zap.DebugLevel, "allocated a full GPU"); l != nil {
					// Potentially a very long string so hidden under "log Enabled".
					l.Write(zap.Strings("metagpu_list", devLoad.metagpus))
				}

				// remove current available metagpus from the device load map
				devLoad.metagpus = nil
				// entire gpu allocated, continue to the next entire gpu if needed
				break
			}
		}
	}

	if gpuFractionsRequest > 0 {
		for _, devLoad := range a.loadMap {
			if devLoad == nil {
				continue
			}
			if devLoad.getFreeShares() >= gpuFractionsRequest {
				var devicesToAdd []string
				for i, device := range devLoad.metagpus {
					if i == gpuFractionsRequest {
						break
					}

					if l := logger.Check(zap.DebugLevel, "allocated part of a GPU"); l != nil {
						l.Write(zap.String("metagpu_device", device))
					}

					devicesToAdd = append(devicesToAdd, device)
				}
				a.metagpusAllocations = append(a.metagpusAllocations, devicesToAdd...)
				devLoad.removeDevices(devicesToAdd)
				break
			}
		}
	}
	// if still missing allocations,
	// meaning wasn't able to allocate required fractions from the same GPU
	// will try to allocate a fractions from different GPUs
	allocationsLeft := a.allocationSize - len(a.metagpusAllocations)

	if allocationsLeft > 0 {
		for _, devLoad := range a.loadMap {
			if devLoad == nil {
				continue
			}

			for _, device := range devLoad.metagpus {
				a.metagpusAllocations = append(a.metagpusAllocations, device)
				allocationsLeft--

				if allocationsLeft == 0 {
					goto ExitMultiGpuFractionAlloc
				}
			}
		}
	}
ExitMultiGpuFractionAlloc:

	// After all attempts we did not manage to allocate enough GPUs.
	if len(a.metagpusAllocations) != a.allocationSize {
		logger.With(zap.Int("allocation_size", a.allocationSize)).
			With(zap.Int("allocated_devices", len(a.metagpusAllocations))).
			Error("error during allocation, the allocationSize doesn't match total allocated devices")
	}
}

func (l *DeviceLoad) getFreeShares() int {
	return len(l.metagpus)
}

func (l *DeviceLoad) removeDevices(devIds []string) {
	for _, devId := range devIds {
		for i, v := range l.metagpus {
			if v == devId {
				l.metagpus = append(l.metagpus[:i], l.metagpus[i+1:]...)
			}
		}
	}
}

// metaDeviceIdToDeviceIndex - extracts the value of the first "number" from a metaDeviceId.
// An example of the metaDeviceId is "mortalgpu-meta-0-229-MIG-72bdef0f-5d81-52f6-be55-d662a1eadbad"
// from which the function will return 0 which is an NVML device's index.
func metaDeviceIdToDeviceIndex(logger *zap.Logger, metaDeviceId string) (int, error) {
	match := reMetaDeviceID.FindStringSubmatch(metaDeviceId)
	if len(match) < 2 {
		return 0, fmt.Errorf("metaDeviceIdToDeviceIndex: Incorrect MetaDeviceID %q, cannot extract device index", metaDeviceId)
	}

	idx, err := strconv.Atoi(match[1])
	if err != nil {
		logger.With(zap.Error(err)).Error("can't detect physical device ID from meta device id")

		return 0, fmt.Errorf("metaDeviceIdToDeviceIndex: Incorrect MetaDeviceID %q, not integer", match[1])
	}
	return idx, nil
}
