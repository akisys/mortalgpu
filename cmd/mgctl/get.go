package main

import (
	"encoding/json"
	"fmt"
	"io"
	"os"
	"os/signal"
	"syscall"
	"time"

	"atomicgo.dev/cursor"
	"github.com/jedib0t/go-pretty/v6/table"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	pbdevice "gitlab.com/MaxIV/kubernetes/mortalgpu/gen/proto/go/device/v1"
	"gitlab.com/MaxIV/kubernetes/mortalgpu/pkg/ctlutils"
	"go.uber.org/zap"
)

const (
	ColorCritical = "\u001B[31m"
	ColorGood     = "\u001B[32m"
	ColorWarning  = "\u001B[33m"
	ColorNeutral  = "\u001B[0m"
)

var getCmd = &cobra.Command{
	Use:     "get",
	Aliases: []string{"g"},
	Short:   "get resources",
}

var processGetParams = []param{
	{name: "watch", shorthand: "w", value: false, usage: "watch for the changes"},
}

var processesGetCmd = &cobra.Command{
	Use:     "processes",
	Aliases: []string{"p", "process"},
	Short:   "list gpu processes and processes metadata",
	Run: func(cmd *cobra.Command, args []string) {
		getDevicesProcesses(zap.L().Named("get_processes"))
	},
}

var getDevicesCmd = &cobra.Command{
	Use:     "devices",
	Aliases: []string{"d", "device"},
	Short:   "get gpu devices",
	Run: func(cmd *cobra.Command, args []string) {
		getDevices(zap.L().Named("get_devices"))
	},
}

type enrichedProcess struct {
	*pbdevice.DeviceProcess
	RelativeGpuUsagePercent *uint32 `json:"relative_gpu_usage_percent"` // Can be JSON null.
	ComputeInstanceId       *int64  `json:"compute_instance_id"`
	MigDevice               *int64  `json:"mig_device"`
}

type enrichedGPUContainer struct {
	*pbdevice.GpuContainer
	DeviceProcesses []*enrichedProcess `json:"device_processes,omitempty"`
}

func getDevices(logger *zap.Logger) {
	conn := ctlutils.GetGrpcMetaGpuSrvClientConn(logger, viper.GetString(flagAddr))
	if conn == nil {
		logger.Fatal("can't initiate connection to metagpu server", zap.String("server", viper.GetString(flagAddr)))
	}
	defer conn.Close()
	device := pbdevice.NewDeviceServiceClient(conn)
	resp, err := device.GetMetaDeviceInfo(ctlutils.AuthenticatedContext(viper.GetString("token")), &pbdevice.GetMetaDeviceInfoRequest{})
	if err != nil {
		logger.Fatal("can't GetMetaDeviceInfo", zap.Error(err))
	}

	if zap.DebugLevel.Enabled(logger.Level()) {
		logger.Debug("GRPC response", zap.Any("response", resp))
	}

	var printer devicesPrinter

	switch of := viper.GetString(flagOutput); of {
	case outTable:
		printer = &devicesPrinterTable{}
	case outJSON:
		printer = &devicesPrinterJSON{pretty: viper.GetBool(flagPrettyOut)}
	case outRaw:
		printer = &devicesPrinterRAW{}
	default:
		logger.Fatal("Output format is not supported", zap.String("format", of))
	}

	if err := printer.print(resp.Devices); err != nil {
		logger.Fatal("can't format output", zap.Error(err))
	}
}

func getDevicesProcesses(logger *zap.Logger) {

	conn := ctlutils.GetGrpcMetaGpuSrvClientConn(logger, viper.GetString(flagAddr))
	if conn == nil {
		logger.Fatal("can't initiate connection to metagpu server", zap.String("server", viper.GetString(flagAddr)))
	}
	defer conn.Close()
	device := pbdevice.NewDeviceServiceClient(conn)
	hostname, err := os.Hostname()
	if err != nil {
		logger.Error("failed to detect podId", zap.Error(err))
	}

	var printer deviceProcessesPrinter

	switch of := viper.GetString(flagOutput); of {
	case outTable:
		printer = &deviceProcessesPrinterTable{}
	case outJSON:
		printer = &deviceProcessesPrinterJSON{pretty: viper.GetBool(flagPrettyOut)}
	case outRaw:
		printer = &deviceProcessesPrinterRAW{}
	default:
		logger.Fatal("Output format is not supported", zap.String("format", of))
	}

	if viper.GetBool("watch") {
		request := &pbdevice.StreamGpuContainersRequest{PodId: hostname}
		stream, err := device.StreamGpuContainers(ctlutils.AuthenticatedContext(viper.GetString("token")), request)
		if err != nil {
			logger.Fatal("can't StreamGpuContainers", zap.Error(err))
		}

		refreshCh := make(chan bool)
		sigCh := make(chan os.Signal, 1)
		signal.Notify(sigCh, syscall.SIGHUP, syscall.SIGINT, syscall.SIGTERM, syscall.SIGQUIT)

		go func() {
			for {
				time.Sleep(1 * time.Second)
				refreshCh <- true
			}
		}()

		for {
			select {
			case <-sigCh:
				cursor.ClearLine()
				logger.Info("shutting down")
				os.Exit(0)
			case <-refreshCh:
				processResp, err := stream.Recv()
				if err == io.EOF {
					break
				}
				if err != nil {
					logger.Fatal("error watching gpu processes", zap.Error(err))
				}
				deviceResp, err := device.GetDevices(ctlutils.AuthenticatedContext(viper.GetString("token")), &pbdevice.GetDevicesRequest{})
				if err != nil {
					logger.Error("failed to list devices", zap.Error(err))
					return
				}

				if zap.DebugLevel.Enabled(logger.Level()) {
					logger.Debug("GRPC response", zap.Any("process_response", processResp), zap.Any("device_response", deviceResp))
				}

				if err := printer.print(processResp.GpuContainers, deviceResp.Device, processResp.VisibilityLevel); err != nil {
					logger.Fatal("can't format output", zap.Error(err))
				}
			}
		}
	} else {
		processResp, err := device.GetGpuContainers(ctlutils.AuthenticatedContext(viper.GetString("token")), &pbdevice.GetGpuContainersRequest{PodId: hostname})
		if err != nil {
			logger.Error("failed to list device processes", zap.Error(err))
			return
		}
		deviceResp, err := device.GetDevices(ctlutils.AuthenticatedContext(viper.GetString("token")), &pbdevice.GetDevicesRequest{})
		if err != nil {
			logger.Error("failed to list devices", zap.Error(err))
			return
		}

		if zap.DebugLevel.Enabled(logger.Level()) {
			logger.Debug("GRPC response", zap.Any("process_response", processResp), zap.Any("device_response", deviceResp))
		}

		if err := printer.print(processResp.GpuContainers, deviceResp.Device, processResp.VisibilityLevel); err != nil {
			logger.Fatal("can't format output", zap.Error(err))
		}
	}
}

// devicesPrinter interface to print GPU devices.
type devicesPrinter interface {
	print([]*pbdevice.Device) error
}

// devicesPrinterTable - printer with human-readable table output format.
type devicesPrinterTable struct {
	to *TableOutput
}

// Check interface compatibility.
var _ devicesPrinter = (*devicesPrinterTable)(nil)

func (dpt *devicesPrinterTable) print(devices []*pbdevice.Device) error {
	if dpt.to == nil {
		dpt.to = &TableOutput{}
		dpt.to.header = table.Row{"Idx", "UUID", "Memory", "Shares", "Share size"}
	}

	dpt.to.body, dpt.to.footer = dpt.buildDeviceInfoTableBody(devices)
	dpt.to.buildTable()
	dpt.to.print()

	return nil
}

// buildDeviceInfoTableBody adds rows to the resulting table.
func (dpt *devicesPrinterTable) buildDeviceInfoTableBody(devices []*pbdevice.Device) (body []table.Row, footer table.Row) {
	var totMem uint64
	var shares uint32
	for _, d := range devices {
		shares = d.Shares
		totMem += d.MemoryTotal
		body = append(body, table.Row{
			d.Index,
			d.Uuid,
			d.MemoryTotal,
			d.Shares,
			d.MemoryShareSize,
		})
	}
	footer = table.Row{len(devices), "", fmt.Sprintf("%dMB", totMem), uint32(len(devices)) * shares, ""}
	return body, footer
}

// devicesPrinterTable - printer with JSON and indented JSON output formats.
type devicesPrinterJSON struct {
	pretty bool
}

// Check interface compatibility.
var _ devicesPrinter = (*devicesPrinterJSON)(nil)

func (dpt *devicesPrinterJSON) print(devices []*pbdevice.Device) error {
	var (
		bts []byte
		err error
	)

	if dpt.pretty {
		bts, err = json.MarshalIndent(devices, "", "  ")
	} else {
		bts, err = json.Marshal(devices)
	}
	if err != nil {
		return err
	}

	println(string(bts))

	return nil
}

// devicesPrinterRAW  - printer with Go internal output format.
type devicesPrinterRAW struct{}

// Check interface compatibility.
var _ devicesPrinter = (*devicesPrinterRAW)(nil)

func (dpt *devicesPrinterRAW) print(devices []*pbdevice.Device) error {
	_, err := fmt.Printf("%+v\n", devices)

	return err
}

// deviceProcessesPrinter interface to print GPU devices processes.
type deviceProcessesPrinter interface {
	print(containers []*pbdevice.GpuContainer, devices map[string]*pbdevice.Device, vl string) error
}

// deviceProcessesPrinterTable - printer with human-readable table output format.
type deviceProcessesPrinterTable struct {
	to *TableOutput
}

// Check interface compatibility.
var _ deviceProcessesPrinter = (*deviceProcessesPrinterTable)(nil)

func (dpt *deviceProcessesPrinterTable) print(containers []*pbdevice.GpuContainer, devices map[string]*pbdevice.Device, vl string) error {
	// First call - add header.
	if dpt.to == nil {
		dpt.to = &TableOutput{}
		dpt.to.header = table.Row{"Pod", "NS", "Device", "Node", "GPU", "Memory", "Pid", "Cmd", "Req/Limits"}
	}

	dpt.to.body = dpt.buildDeviceProcessesTableBody(containers)
	dpt.to.footer = dpt.buildDeviceProcessesTableFooter(containers, devices, vl)
	dpt.to.buildTable()
	dpt.to.print()

	return nil
}

// buildDeviceProcessesTableBody - adds rows to the resulting table.
func (dpt *deviceProcessesPrinterTable) buildDeviceProcessesTableBody(containers []*pbdevice.GpuContainer) (body []table.Row) {

	for _, c := range containers {
		if len(c.ContainerDevices) > 0 {
			maxMem := int64(c.ContainerDevices[0].Device.MemoryShareSize * uint64(c.MetagpuLimits))
			reqMem := int64(c.ContainerDevices[0].Device.MemoryShareSize * uint64(c.MetagpuRequests))
			if len(c.DeviceProcesses) > 0 {
				for _, p := range c.DeviceProcesses {
					gpuUsageColor := ColorGood
					relativeGpuUsage := uint32(float64(p.GpuUtilization*c.ContainerDevices[0].Device.Shares) / float64(c.MetagpuLimits))
					requestGpuUsageTreshold := 100 * uint32(c.MetagpuRequests) / uint32(c.MetagpuLimits)
					if relativeGpuUsage > requestGpuUsageTreshold {
						gpuUsageColor = ColorWarning
					}
					if relativeGpuUsage > 100 {
						gpuUsageColor = ColorCritical
					}
					gpuUsage := fmt.Sprintf("%s%d%%%s", gpuUsageColor, relativeGpuUsage, ColorNeutral)
					memUsageColor := ColorGood
					if int64(p.Memory) > reqMem {
						memUsageColor = ColorWarning
					}
					if int64(p.Memory) > maxMem {
						memUsageColor = ColorCritical
					}
					memUsage := fmt.Sprintf("%s%d%s/%d/%d", memUsageColor, p.Memory, ColorNeutral, reqMem, maxMem)
					body = append(body, table.Row{
						c.PodId,
						c.PodNamespace,
						formatContainerDeviceIndexes(c),
						c.NodeName,
						gpuUsage,
						memUsage,
						p.Pid,
						p.Cmdline,
						fmt.Sprintf("%d/%d", c.MetagpuRequests, c.MetagpuLimits),
					})
				}
			} else {
				memUsage := fmt.Sprintf("%s%d%s/%d/%d", ColorGood, 0, ColorNeutral, reqMem, maxMem)
				body = append(body, table.Row{
					c.PodId,
					c.PodNamespace,
					formatContainerDeviceIndexes(c),
					c.NodeName,
					"-",
					memUsage,
					"-",
					"-",
					fmt.Sprintf("%d/%d", c.MetagpuRequests, c.MetagpuLimits),
				})
			}
		} else {
			body = append(body, table.Row{
				c.PodId,
				c.PodNamespace,
				formatContainerDeviceIndexes(c),
				c.NodeName,
				"-",
				"-",
				"-",
				"-",
				fmt.Sprintf("%d/%d", c.MetagpuRequests, c.MetagpuLimits),
			})
		}

	}

	return
}

func (dpt *deviceProcessesPrinterTable) buildDeviceProcessesTableFooter(containers []*pbdevice.GpuContainer, devices map[string]*pbdevice.Device, vl string) (footer table.Row) {
	// metaGpuSummary := fmt.Sprintf("%d/%d", getTotalRequests(containers), getTotalLimits(containers))
	// TODO: fix this, the vl should be taken from directly form the  package
	// to problem is that package now includes the nvidia linux native stuff
	// and some package re-org is required
	//if vl == "l0" { // TODO: temporary disabled
	metaGpuSummary := fmt.Sprintf("%d/%d/%d", getTotalShares(devices), getTotalRequests(containers), getTotalLimits(containers))
	//}
	usedMem := fmt.Sprintf("%dMb", getTotalMemoryUsedByProcesses(containers))
	return table.Row{len(containers), "", "", "", "", usedMem, "", "", metaGpuSummary}
}

func encrichGPUContainers(containers []*pbdevice.GpuContainer) []*enrichedGPUContainer {
	var result = make([]*enrichedGPUContainer, 0, len(containers))

	if zap.DebugLevel.Enabled(zap.L().Level()) {
		zap.L().Debug("Printing GPU containers")
	}

	for _, c := range containers {
		enrCnt := &enrichedGPUContainer{
			GpuContainer: c,
		}

		var logger *zap.Logger

		if zap.DebugLevel.Enabled(zap.L().Level()) {
			logger = zap.L().With(
				zap.String("namespace", c.GetPodNamespace()),
				zap.String("pod_id", c.GetPodId()),
				zap.String("container", c.GetContainerName()))

			logger.Debug("Container processes")
		}

		for _, p := range c.DeviceProcesses {
			if len(c.ContainerDevices) > 0 {
				if zap.DebugLevel.Enabled(zap.L().Level()) {
					logger.Debug("Process", zap.Any("device_process", p))
				}

				relativeGpuUsage := uint32(float64(p.GpuUtilization*c.ContainerDevices[0].Device.Shares) / float64(c.MetagpuLimits))

				enrCnt.DeviceProcesses = append(enrCnt.DeviceProcesses, &enrichedProcess{
					DeviceProcess:           p,
					RelativeGpuUsagePercent: &relativeGpuUsage,
					ComputeInstanceId:       &p.ComputeInstanceId,
					MigDevice:               &p.MigDevice,
				})
			} else {
				enrCnt.DeviceProcesses = append(enrCnt.DeviceProcesses, &enrichedProcess{
					DeviceProcess:           p,
					RelativeGpuUsagePercent: nil,
					ComputeInstanceId:       &p.ComputeInstanceId,
					MigDevice:               &p.MigDevice,
				})
			}
		}

		result = append(result, enrCnt)
	}

	return result
}

// deviceProcessesPrinterJSON - printer with JSON and indented JSON output format.
type deviceProcessesPrinterJSON struct {
	pretty bool
}

// Check interface compatibility.
var _ deviceProcessesPrinter = (*deviceProcessesPrinterJSON)(nil)

func (dpt *deviceProcessesPrinterJSON) print(containers []*pbdevice.GpuContainer, _ map[string]*pbdevice.Device, vl string) error {
	var result = encrichGPUContainers(containers)

	var (
		bts []byte
		err error
	)

	if dpt.pretty {
		bts, err = json.MarshalIndent(result, "", "  ")
	} else {
		bts, err = json.Marshal(result)
	}
	if err != nil {
		return err
	}

	println(string(bts))

	return nil
}

// deviceProcessesPrinterRAW - printer with JSON and indented JSON output format.
type deviceProcessesPrinterRAW struct{}

// Check interface compatibility.
var _ deviceProcessesPrinter = (*deviceProcessesPrinterRAW)(nil)

func (dpt *deviceProcessesPrinterRAW) print(containers []*pbdevice.GpuContainer, _ map[string]*pbdevice.Device, vl string) error {
	var result = encrichGPUContainers(containers)

	_, err := fmt.Printf("%+v\n", result)
	return err
}
