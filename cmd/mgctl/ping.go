package main

import (
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/MaxIV/kubernetes/mortalgpu/pkg/ctlutils"
	"go.uber.org/zap"
)

var pingCmd = &cobra.Command{
	Use:   "ping",
	Short: "ping server to check connectivity",
	Run: func(cmd *cobra.Command, args []string) {
		conn := ctlutils.GetGrpcMetaGpuSrvClientConn(zap.L(), viper.GetString(flagAddr))
		if conn == nil {
			zap.L().Named("ping").Fatal("can't initiate connection to metagpu server", zap.String("server", viper.GetString(flagAddr)))
		}
		defer conn.Close()
	},
}
