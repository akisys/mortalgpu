package main

import (
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	pbdevice "gitlab.com/MaxIV/kubernetes/mortalgpu/gen/proto/go/device/v1"
	"gitlab.com/MaxIV/kubernetes/mortalgpu/pkg/ctlutils"
	"go.uber.org/zap"
)

var (
	configCmdParams = []param{
		{name: "metagpu", shorthand: "m", value: 0, usage: "set metagpus quantity (gpu shares)"},
		{name: "auto", shorthand: "a", value: false, usage: "automatically configure GPU shares"},
	}
)

var configCmd = &cobra.Command{
	Use:   "config",
	Short: "change configs on running metagpu device plugin instance",
	Run: func(cmd *cobra.Command, args []string) {
		patchConfigs(zap.L().Named("config"))
	},
}

func patchConfigs(logger *zap.Logger) {
	if viper.GetInt32("metagpu") != 0 {
		logger = logger.Named("patchConfigs")
		metaGpus := viper.GetInt32("metagpu")

		if zap.DebugLevel.Enabled(logger.Level()) {
			logger.Debug("MetaGPUs", zap.Int32("count", metaGpus))
		}

		conn := ctlutils.GetGrpcMetaGpuSrvClientConn(zap.L(), viper.GetString(flagAddr))
		if conn == nil {
			logger.Fatal("can't initiate connection to metagpu server", zap.String("server", viper.GetString(flagAddr)))
		}
		defer conn.Close()
		device := pbdevice.NewDeviceServiceClient(conn)

		request := &pbdevice.PatchConfigsRequest{MetaGpus: metaGpus}
		if _, err := device.PatchConfigs(ctlutils.AuthenticatedContext(viper.GetString("token")), request); err != nil {
			logger.Error("can't patch configs", zap.Error(err))
		}
	}
}
