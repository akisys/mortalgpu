package main

import (
	"errors"
	"fmt"
	"net"
	"net/http"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/spf13/viper"
	pbdevice "gitlab.com/MaxIV/kubernetes/mortalgpu/gen/proto/go/device/v1"
	"gitlab.com/MaxIV/kubernetes/mortalgpu/pkg/ctlutils"
	"go.uber.org/zap"
	"google.golang.org/grpc"
)

var (
	conn         *grpc.ClientConn
	devicesCache map[string]*pbdevice.Device

	deviceShares = prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Namespace: "metagpu",
		Subsystem: "device",
		Name:      "shares",
		Help:      "total shares for single gpu unit",
	}, []string{"device_uuid", "device_index", "resource_name", "node_name"})

	deviceAllocatedShares = prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Namespace: "metagpu",
		Subsystem: "device",
		Name:      "allocated_shares",
		Help:      "allocated shares for single gpu unit",
	}, []string{"device_uuid", "device_index", "resource_name", "node_name"})

	deviceMemTotal = prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Namespace: "metagpu",
		Subsystem: "device",
		Name:      "memory_total",
		Help:      "total memory per device",
	}, []string{"device_uuid", "device_index", "resource_name", "node_name"})

	deviceMemFree = prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Namespace: "metagpu",
		Subsystem: "device",
		Name:      "memory_free",
		Help:      "free memory per device",
	}, []string{"device_uuid", "device_index", "resource_name", "node_name"})

	deviceMemUsed = prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Namespace: "metagpu",
		Subsystem: "device",
		Name:      "memory_used",
		Help:      "used memory per device",
	}, []string{"device_uuid", "device_index", "resource_name", "node_name"})

	deviceMemShareSize = prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Namespace: "metagpu",
		Subsystem: "device",
		Name:      "memory_share_size",
		Help:      "metagpu memory share size",
	}, []string{"device_uuid", "device_index", "resource_name", "node_name"})

	deviceProcessAbsoluteGpuUtilization = prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Namespace: "metagpu",
		Subsystem: "process",
		Name:      "absolute_gpu_utilization",
		Help:      "gpu process utilization in percentage",
	}, []string{"uuid", "pid", "cmdline", "user", "pod_name", "pod_namespace", "resource_name", "node_name"})

	deviceProcessMemoryUsage = prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Namespace: "metagpu",
		Subsystem: "process",
		Name:      "memory_usage",
		Help:      "process gpu-memory usage",
	}, []string{"uuid", "pid", "cmdline", "user", "pod_name", "pod_namespace", "resource_name", "node_name"})

	deviceProcessMetagpuRequests = prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Namespace: "metagpu",
		Subsystem: "process",
		Name:      "metagpu_requests",
		Help:      "total metagpu requests in deployment spec",
	}, []string{"pod_name", "pod_namespace", "resource_name", "node_name"})

	deviceProcessMetagpuLimits = prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Namespace: "metagpu",
		Subsystem: "process",
		Name:      "metagpu_limits",
		Help:      "total metagpu limits in deployment spec",
	}, []string{"pod_name", "pod_namespace", "resource_name", "node_name"})

	deviceProcessRequestedMetagpuGPUUtilization = prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Namespace: "metagpu",
		Subsystem: "process",
		Name:      "requested_metagpu_gpu_utilization",
		Help:      "requested metagpu gpu utilization",
	}, []string{"uuid", "pid", "cmdline", "user", "pod_name", "pod_namespace", "resource_name", "node_name"})

	deviceProcessMaxAllowedMetagpuGPUUtilization = prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Namespace: "metagpu",
		Subsystem: "process",
		Name:      "max_allowed_metagpu_gpu_utilization",
		Help:      "max allowed metagpu gpu utilization",
	}, []string{"uuid", "pid", "cmdline", "user", "pod_name", "pod_namespace", "resource_name", "node_name"})

	deviceProcessMetagpuRelativeGPUUtilization = prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Namespace: "metagpu",
		Subsystem: "process",
		Name:      "metagpu_relative_gpu_utilization",
		Help:      "relative to metagpu limits gpu utilization",
	}, []string{"uuid", "pid", "cmdline", "user", "pod_name", "pod_namespace", "resource_name", "node_name"})

	deviceProcessRequestedMetaGpuMemory = prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Namespace: "metagpu",
		Subsystem: "process",
		Name:      "requested_metagpu_memory",
		Help:      "requested metagpu memory usage",
	}, []string{"uuid", "pid", "cmdline", "user", "pod_name", "pod_namespace", "resource_name", "node_name"})

	deviceProcessMaxAllowedMetaGpuMemory = prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Namespace: "metagpu",
		Subsystem: "process",
		Name:      "max_allowed_metagpu_memory",
		Help:      "max allowed metagpu memory usage",
	}, []string{"uuid", "pid", "cmdline", "user", "pod_name", "pod_namespace", "resource_name", "node_name"})

	deviceProcessMetagpuRelativeMemoryUtilization = prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Namespace: "metagpu",
		Subsystem: "process",
		Name:      "metagpu_relative_memory_utilization",
		Help:      "relative to metagpus limits memory utilization",
	}, []string{"uuid", "pid", "cmdline", "user", "pod_name", "pod_namespace", "resource_name", "node_name"})
)

func getGpuContainers(logger *zap.Logger) []*pbdevice.GpuContainer {
	devices := pbdevice.NewDeviceServiceClient(conn)
	req := &pbdevice.GetGpuContainersRequest{}
	ctx := ctlutils.AuthenticatedContext(viper.GetString("token"))
	resp, err := devices.GetGpuContainers(ctx, req)
	if err != nil {
		logger.With(zap.Error(err)).Error("can't get GPU Containers")
		return nil
	}
	return resp.GpuContainers
}

func getGpuDevicesInfo(logger *zap.Logger) []*pbdevice.Device {
	devices := pbdevice.NewDeviceServiceClient(conn)
	req := &pbdevice.GetMetaDeviceInfoRequest{}
	ctx := ctlutils.AuthenticatedContext(viper.GetString("token"))
	resp, err := devices.GetMetaDeviceInfo(ctx, req)
	if err != nil {
		logger.With(zap.Error(err)).Error("can't get GPU Devices Info")
		return nil
	}
	return resp.Devices
}

func setGpuDevicesCache(logger *zap.Logger) map[string]*pbdevice.Device {
	if devicesCache != nil {
		return devicesCache
	}
	devicesCache = make(map[string]*pbdevice.Device)
	devices := pbdevice.NewDeviceServiceClient(conn)
	req := &pbdevice.GetDevicesRequest{}
	ctx := ctlutils.AuthenticatedContext(viper.GetString("token"))
	resp, err := devices.GetDevices(ctx, req)
	if err != nil {
		logger.With(zap.Error(err)).Error("can't get GPU Devices")
		return nil
	}

	devicesCache = resp.Device
	return devicesCache
}

func clearGpuDevicesCache() {
	devicesCache = nil
}

func setDevicesMetrics(logger *zap.Logger) {
	// GPU device metrics
	for _, d := range getGpuDevicesInfo(logger) {
		labels := []string{d.Uuid, fmt.Sprintf("%d", d.Index), d.ResourceName, d.NodeName}
		deviceShares.WithLabelValues(labels...).Set(float64(d.Shares))
		deviceAllocatedShares.WithLabelValues(labels...).Set(float64(d.AllocatedShares))
		deviceMemTotal.WithLabelValues(labels...).Set(float64(d.MemoryTotal))
		deviceMemFree.WithLabelValues(labels...).Set(float64(d.MemoryFree))
		deviceMemUsed.WithLabelValues(labels...).Set(float64(d.MemoryUsed))
		deviceMemShareSize.WithLabelValues(labels...).Set(float64(d.MemoryShareSize))
	}
}

func resetProcessLevelMetrics() {
	deviceProcessAbsoluteGpuUtilization.Reset()
	deviceProcessMemoryUsage.Reset()
	deviceProcessMetagpuRequests.Reset()
	deviceProcessMetagpuLimits.Reset()
	deviceProcessRequestedMetagpuGPUUtilization.Reset()
	deviceProcessMaxAllowedMetagpuGPUUtilization.Reset()
	deviceProcessMetagpuRelativeGPUUtilization.Reset()
	deviceProcessRequestedMetaGpuMemory.Reset()
	deviceProcessMaxAllowedMetaGpuMemory.Reset()
	deviceProcessMetagpuRelativeMemoryUtilization.Reset()
}

func setProcessesMetrics(logger *zap.Logger) {
	// reset metrics
	resetProcessLevelMetrics()
	// GPU processes metrics
	for _, c := range getGpuContainers(logger) {
		// metagpu requests
		deviceProcessMetagpuRequests.WithLabelValues(
			c.PodId, c.PodNamespace, c.ResourceName, c.NodeName).Set(float64(c.MetagpuRequests))
		// metagpu limits
		deviceProcessMetagpuLimits.WithLabelValues(
			c.PodId, c.PodNamespace, c.ResourceName, c.NodeName).Set(float64(c.MetagpuLimits))
		// if pod has processes expose process metrics
		if len(c.DeviceProcesses) > 0 {
			for _, p := range c.DeviceProcesses {
				// set labels for device process level metrics
				labels := []string{
					p.Devuuid, fmt.Sprintf("%d", p.Pid), p.Cmdline, p.User, c.PodId, c.PodNamespace, c.ResourceName, c.NodeName}
				// absolute memory and gpu usage
				deviceProcessAbsoluteGpuUtilization.WithLabelValues(labels...).Set(float64(p.GpuUtilization))
				deviceProcessMemoryUsage.WithLabelValues(labels...).Set(float64(p.Memory))
				// requested (relavive to metagpu requests) gpu and memory utilization
				deviceProcessRequestedMetagpuGPUUtilization.WithLabelValues(labels...).Set(getRequestedMetagpuGPUUtilization(logger, c))
				deviceProcessRequestedMetaGpuMemory.WithLabelValues(labels...).Set(getRequestedMetaGpuMemory(logger, c))
				// max allowed (relative to metagpus limits) gpu and memory utilization
				deviceProcessMaxAllowedMetagpuGPUUtilization.WithLabelValues(labels...).Set(getMaxAllowedMetagpuGPUUtilization(logger, c))
				deviceProcessMaxAllowedMetaGpuMemory.WithLabelValues(labels...).Set(getMaxAllowedMetaGpuMemory(logger, c))
				// relative to limits gpu and memory utilization
				deviceProcessMetagpuRelativeGPUUtilization.WithLabelValues(labels...).Set(getRelativeGPUUtilization(logger, c, p))
				deviceProcessMetagpuRelativeMemoryUtilization.WithLabelValues(labels...).Set(getRelativeMemoryUtilization(logger, c, p))
			}
		} else { // pod doesn't have any processes, all the metrics should be set to 0
			labels := []string{
				"-", "-", "-", "-", c.PodId, c.PodNamespace, c.ResourceName, c.NodeName}
			// absolute memory and gpu usage
			deviceProcessAbsoluteGpuUtilization.WithLabelValues(labels...).Set(0)
			deviceProcessMemoryUsage.WithLabelValues(labels...).Set(0)
			// max (relative to metagpus request) allowed gpu and memory utilization
			deviceProcessMaxAllowedMetagpuGPUUtilization.WithLabelValues(labels...).Set(0)
			deviceProcessMaxAllowedMetaGpuMemory.WithLabelValues(labels...).Set(0)
			// relative gpu and memory utilization
			deviceProcessMetagpuRelativeGPUUtilization.WithLabelValues(labels...).Set(0)
			deviceProcessMetagpuRelativeMemoryUtilization.WithLabelValues(labels...).Set(0)
		}
	}
}

func getMaxAllowedMetagpuGPUUtilization(logger *zap.Logger, c *pbdevice.GpuContainer) float64 {
	d, err := getFirstContainerDevice(c)
	if err != nil {
		logger.Sugar().Errorw("can't get max allowed Metagpu GPU utilization",
			"pod", c.PodId,
			"pod_namespace", c.PodNamespace,
			"resource_name", c.ResourceName,
			"node_name", c.NodeName,
			"container_name", c.ContainerName,
			"container_id", c.ContainerId)
		return 0
	}
	return float64((100 / d.Device.Shares) * uint32(c.MetagpuLimits))
}

func getMaxAllowedMetaGpuMemory(logger *zap.Logger, c *pbdevice.GpuContainer) float64 {
	d, err := getFirstContainerDevice(c)
	if err != nil {
		logger.Sugar().Errorw("can't get max allowed Metagpu memory",
			"pod", c.PodId,
			"pod_namespace", c.PodNamespace,
			"resource_name", c.ResourceName,
			"node_name", c.NodeName,
			"container_name", c.ContainerName,
			"container_id", c.ContainerId)
		return 0
	}
	return float64(uint64(c.MetagpuLimits) * d.Device.MemoryShareSize)

}

func getRequestedMetagpuGPUUtilization(logger *zap.Logger, c *pbdevice.GpuContainer) float64 {
	d, err := getFirstContainerDevice(c)
	if err != nil {
		logger.Sugar().Errorw("can't get requested metagpu GPU Utilization",
			"pod", c.PodId,
			"pod_namespace", c.PodNamespace,
			"resource_name", c.ResourceName,
			"node_name", c.NodeName,
			"container_name", c.ContainerName,
			"container_id", c.ContainerId)
		return 0
	}
	return float64((100 / d.Device.Shares) * uint32(c.MetagpuRequests))
}

func getRequestedMetaGpuMemory(logger *zap.Logger, c *pbdevice.GpuContainer) float64 {
	d, err := getFirstContainerDevice(c)
	if err != nil {
		logger.Sugar().Errorw("can't get requested Metagpu memory",
			"pod", c.PodId,
			"pod_namespace", c.PodNamespace,
			"resource_name", c.ResourceName,
			"node_name", c.NodeName,
			"container_name", c.ContainerName,
			"container_id", c.ContainerId)
		return 0
	}
	return float64(uint64(c.MetagpuRequests) * d.Device.MemoryShareSize)

}

func getFirstContainerDevice(c *pbdevice.GpuContainer) (*pbdevice.ContainerDevice, error) {
	if len(c.ContainerDevices) == 0 {
		return nil, errors.New("no allocated gpus found")
	}
	return c.ContainerDevices[0], nil
}

func getRelativeGPUUtilization(logger *zap.Logger, c *pbdevice.GpuContainer, p *pbdevice.DeviceProcess) float64 {
	d, err := getFirstContainerDevice(c)
	if err != nil {
		logger.Sugar().Errorw("can't get MaxAllowedMetagpu GPU Utilization",
			"pod", c.PodId,
			"pod_namespace", c.PodNamespace,
			"resource_name", c.ResourceName,
			"node_name", c.NodeName,
			"container_name", c.ContainerName,
			"container_id", c.ContainerId,
			"error", err)
		return 0
	}
	maxMetaGpuUtilization := (100 / d.Device.Shares) * uint32(c.MetagpuLimits)
	metaGpuUtilization := 0
	if p.GpuUtilization > 0 && maxMetaGpuUtilization > 0 {
		metaGpuUtilization = int((p.GpuUtilization * 100) / maxMetaGpuUtilization)
	}
	return float64(metaGpuUtilization)
}

func getRelativeMemoryUtilization(logger *zap.Logger, c *pbdevice.GpuContainer, p *pbdevice.DeviceProcess) float64 {
	d, err := getFirstContainerDevice(c)
	if err != nil {
		logger.Sugar().Errorw("can't get MaxAllowedMetagpu GPU Utilization",
			"pod", c.PodId,
			"pod_namespace", c.PodNamespace,
			"resource_name", c.ResourceName,
			"node_name", c.NodeName,
			"container_name", c.ContainerName,
			"container_id", c.ContainerId,
			"error", err)
		return 0
	}
	maxMetaMemory := int(uint64(c.MetagpuLimits) * d.Device.MemoryShareSize)
	metaMemUtilization := 0
	if maxMetaMemory > 0 {
		metaMemUtilization = (int(p.Memory) * 100) / maxMetaMemory
	}
	return float64(metaMemUtilization)
}

func recordMetrics(logger *zap.Logger) {
	go func() {
		for {
			conn = ctlutils.GetGrpcMetaGpuSrvClientConn(logger, viper.GetString("mgsrv"))
			if conn == nil {
				logger.Fatal("GRPC connection failed, can't continue")
				continue
			}
			// load devices cache
			setGpuDevicesCache(logger)
			// set devices level metrics
			setDevicesMetrics(logger)
			// set processes level metrics
			setProcessesMetrics(logger)
			// close grcp connections
			conn.Close()
			// clear the cache
			clearGpuDevicesCache()
			time.Sleep(15 * time.Second)
		}
	}()
}

func startExporter(logger *zap.Logger) {
	logger.Info("starting metagpu metrics exporter")

	prometheus.MustRegister(deviceShares)
	prometheus.MustRegister(deviceAllocatedShares)
	prometheus.MustRegister(deviceMemTotal)
	prometheus.MustRegister(deviceMemFree)
	prometheus.MustRegister(deviceMemUsed)
	prometheus.MustRegister(deviceMemShareSize)
	prometheus.MustRegister(deviceProcessAbsoluteGpuUtilization)
	prometheus.MustRegister(deviceProcessMemoryUsage)
	prometheus.MustRegister(deviceProcessMetagpuRequests)
	prometheus.MustRegister(deviceProcessMetagpuLimits)
	prometheus.MustRegister(deviceProcessRequestedMetagpuGPUUtilization)
	prometheus.MustRegister(deviceProcessMaxAllowedMetagpuGPUUtilization)
	prometheus.MustRegister(deviceProcessMetagpuRelativeGPUUtilization)
	prometheus.MustRegister(deviceProcessRequestedMetaGpuMemory)
	prometheus.MustRegister(deviceProcessMaxAllowedMetaGpuMemory)
	prometheus.MustRegister(deviceProcessMetagpuRelativeMemoryUtilization)
	recordMetrics(logger)

	addr := viper.GetString("metrics-addr")
	http.Handle("/metrics", promhttp.Handler())

	l, err := net.Listen("tcp", addr)
	if err != nil {
		logger.With(zap.Error(err)).With(zap.String("socket_addr", addr)).Error("can't bind socket")
		return
	}
	logger.Sugar().Infof("metrics serving on http://%s/metrics", addr)
	if err := http.Serve(l, nil); err != nil {
		logger.With(zap.Error(err)).Error("can't start metrics server")
		return
	}
}
