package main

import (
	"fmt"
	"os"
	"strings"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/MaxIV/kubernetes/mortalgpu/pkg/log"
	"go.uber.org/zap"
)

type param struct {
	name      string
	shorthand string
	value     interface{}
	usage     string
	required  bool
}

var (
	Version string
	Build   string
	rootCmd = &cobra.Command{
		Use:   "mgexporter",
		Short: "mgexporter - Metagpu metrics exporter",
	}
	version = &cobra.Command{
		Use:   "version",
		Short: "Print metagpu metric exporter version and build sha",
		Run: func(cmd *cobra.Command, args []string) {
			fmt.Printf("🐾 version: %s build: %s \n", Version, Build)
		},
	}
	startParams = []param{
		{name: "metrics-addr", shorthand: "a", value: "0.0.0.0:2112", usage: "listen address"},
		{name: "mgsrv", shorthand: "s", value: "127.0.0.1:50052", usage: "metagpu device plugin gRPC server address"},
		{name: "token", shorthand: "t", value: "", usage: "metagpu server authenticate token"},
		{name: "json-log", shorthand: "", value: false, usage: "output logs in json format"},
		{name: "verbose", shorthand: "", value: false, usage: "enable verbose logs"},
	}
	start = &cobra.Command{
		Use:   "start",
		Short: "start metagpu metrics exporter",
		Run: func(cmd *cobra.Command, args []string) {
			startExporter(zap.L())
		},
	}
)

func init() {
	cobra.OnInitialize(initConfig)
	setParams(startParams, start)
	rootCmd.AddCommand(version)
	rootCmd.AddCommand(start)
}

func initConfig() {
	viper.AutomaticEnv()
	viper.SetEnvPrefix("MG_EX")
	viper.SetEnvKeyReplacer(strings.NewReplacer("-", "_"))
	setupLogging()
}

func setParams(params []param, command *cobra.Command) {
	for _, param := range params {
		switch v := param.value.(type) {
		case int:
			command.PersistentFlags().IntP(param.name, param.shorthand, v, param.usage)
		case string:
			command.PersistentFlags().StringP(param.name, param.shorthand, v, param.usage)
		case bool:
			command.PersistentFlags().BoolP(param.name, param.shorthand, v, param.usage)
		}
		if err := viper.BindPFlag(param.name, command.PersistentFlags().Lookup(param.name)); err != nil {
			panic(err)
		}
	}
}

func setupLogging() {
	logger := log.SetupLogging(viper.GetBool("verbose"), viper.GetBool("json-log"), "mgex")

	zap.ReplaceGlobals(logger)
}

func main() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

}
