package main

import (
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"strings"
	"syscall"

	"github.com/fsnotify/fsnotify"
	"github.com/go-logr/logr/funcr"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/MaxIV/kubernetes/mortalgpu/pkg/log"
	"gitlab.com/MaxIV/kubernetes/mortalgpu/pkg/mgsrv"
	"gitlab.com/MaxIV/kubernetes/mortalgpu/pkg/plugin"
	"gitlab.com/MaxIV/kubernetes/mortalgpu/pkg/sharecfg"
	"go.uber.org/zap"
	contrlog "sigs.k8s.io/controller-runtime/pkg/log"
)

type param struct {
	name      string
	shorthand string
	value     interface{}
	usage     string
	required  bool
}

var (
	Version    string
	Build      string
	rootParams = []param{
		{name: "config", shorthand: "c", value: ".", usage: "path to configuration file"},
		{name: "json-log", shorthand: "", value: false, usage: "output logs in json format"},
		{name: "verbose", shorthand: "", value: false, usage: "enable verbose logs"},
		{name: "metrics-port", shorthand: "", value: 2113, usage: "Prometheus metrics exporter port"},
	}
	metaGpuRecalc              = make(chan bool)
	metaGpuDevicePluginVersion = &cobra.Command{
		Use:   "version",
		Short: "Print mortalgpu version and build sha",
		Run: func(cmd *cobra.Command, args []string) {
			fmt.Printf("🐾 version: %s build: %s \n", Version, Build)
		}}
	metaGpuStart = &cobra.Command{
		Use:   "start",
		Short: "Start metagpu device plugin",
		Run: func(cmd *cobra.Command, args []string) {
			var logger = zapLogger.Desugar()

			// Start Prometheus exporter.
			go func() {
				logger.Info("Starting Prometheus metrics exporter",
					zap.String("port", viper.GetString("metrics-port")),
					zap.String("path", "/metrics"),
				)
				http.Handle("/metrics", promhttp.Handler())
				err := http.ListenAndServe(":"+viper.GetString("metrics-port"), nil)
				if err != nil {
					logger.Fatal("Can not start Prometheus metrics exporter", zap.Error(err))
				}
			}()

			var plugins []*plugin.MetaGpuDevicePlugin
			// load gpu shares configuration
			shareConfigs := sharecfg.NewDeviceSharingConfig(logger)
			// init plugins
			for _, c := range shareConfigs.Configs {
				plugins = append(plugins,
					plugin.NewMetaGpuDevicePlugin(
						logger.With(zap.String("device_config_name", c.ResourceName)),
						metaGpuRecalc,
						plugin.NewNvidiaDeviceManager(logger, c)))
			}
			// start plugins
			for _, p := range plugins {
				p.Start()
			}
			// start grpc server
			mgsrv.NewMetaGpuServer(logger).Start()
			// handle interrupts
			sigCh := make(chan os.Signal, 1)
			signal.Notify(sigCh, syscall.SIGHUP, syscall.SIGINT, syscall.SIGTERM, syscall.SIGQUIT)
			for {
				select {
				case s := <-sigCh:
					zapLogger.Infof("signal: %s, shutting down", s)
					// stop all plugins
					for _, p := range plugins {
						p.Stop()
					}
					zapLogger.Info("bye bye 👋")
					os.Exit(0)
				}
			}
		},
	}
	rootCmd = &cobra.Command{
		Use:   "metagpu",
		Short: "Metagpu - fractional accelerator device plugin",
	}
)

var zapLogger *zap.SugaredLogger

func init() {
	cobra.OnInitialize(initConfig)
	setParams(rootParams, rootCmd)
	rootCmd.AddCommand(metaGpuDevicePluginVersion)
	rootCmd.AddCommand(metaGpuStart)
}

func initConfig() {
	viper.AutomaticEnv()
	viper.SetConfigName("config")
	viper.SetConfigType("yaml")
	viper.AddConfigPath("./config")
	viper.AddConfigPath(viper.GetString("config"))
	viper.SetEnvPrefix("METAGPU_DEVICE_PLUGIN")
	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))
	setupLogging()
	err := viper.ReadInConfig()
	if err != nil {
		zapLogger.Fatalw("config file not found", "error", err)
	}
	viper.WatchConfig()
	viper.OnConfigChange(func(e fsnotify.Event) {
		zapLogger.Infof("config file changed, triggering meta gpu recalculation", "file", e.Name)
		metaGpuRecalc <- true
	})
}

func setParams(params []param, command *cobra.Command) {
	for _, param := range params {
		switch v := param.value.(type) {
		case int:
			command.PersistentFlags().IntP(param.name, param.shorthand, v, param.usage)
		case string:
			command.PersistentFlags().StringP(param.name, param.shorthand, v, param.usage)
		case bool:
			command.PersistentFlags().BoolP(param.name, param.shorthand, v, param.usage)
		}
		if err := viper.BindPFlag(param.name, command.PersistentFlags().Lookup(param.name)); err != nil {
			panic(err)
		}
	}
}

func setupLogging() {
	logger := log.SetupLogging(viper.GetBool("verbose"), viper.GetBool("json-log"), "mgdp")

	zapLogger = logger.Sugar()

	zap.ReplaceGlobals(logger)

	var (
		controllerLogger func(args ...interface{})
	)

	if viper.GetBool("verbose") {
		controllerLogger = zapLogger.Named("controller-log").Debug
	} else {
		controllerLogger = zapLogger.Named("controller-log").Info

	}

	contrlog.SetLogger(
		funcr.New(func(prefix, args string) { controllerLogger(prefix, args) },
			funcr.Options{
				LogCaller:     funcr.All,
				LogCallerFunc: true,
			},
		),
	)
}

func main() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
