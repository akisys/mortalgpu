# MortalGPU

[![Container Image](https://img.shields.io/badge/Container%20Image-quay.io-green.svg)](https://quay.io/repository/maxiv/mortalgpu?tab=tags) [![Artifact Hub](https://img.shields.io/endpoint?url=https://artifacthub.io/badge/repository/mortalgpu)](https://artifacthub.io/packages/search?repo=mortalgpu)

MortalGPU is Kubernetes device plugin implementing the concept of sharing Nvidia GPUs memory between workloads.

The development has a strong focus on the workloads run by mortals (such as interactive Jupyter Notebooks) by means of providing a capability for **m**emory **o**ve**r**commi**t**, while maintaining **a**llocation **l**imit per GPU workload - the approach used for sharing RAM on Kubernetes. It also provides visibility on the Pod level for both mortal users and admins.

## Project origins

To achieve flexible GPU sharing for Jupyterhub installation at [MAX IV Laboratory](https://www.maxiv.lu.se) we start evaluating the [MetaGPU Device Plugin](https://github.com/cnvrg/metagpu) that implements the promising idea of dividing the GPU into several meta-GPUs from the Kubernetes scheduling perspective.

MetaGPU was lacking the features we needed (like controlled RAM-like overcommit or MIG support), so we started to implement them and contribute upstream. But as pull requests were not handled for a long time and there were more and more features we wanted to add or rewrite, this process ended up in forking the project and developing MortalGPU as a separate, more feature-rich product.

## MortalGPU vs MetaGPU

While keeping the main operational logic of MetaGPU, MortalGPU adds features and changes the implementation of the logic all over the codebase. The most significant highlights are:

  - Support for memory overcommit with allocation limit, to allow working with GPU RAM the same way Kubernetes works with RAM: using requests for scheduling and limits for enforcing the workload upper usage limit.
  - [Multi-Instance GPU (MIG)](https://www.nvidia.com/en-us/technologies/multi-instance-gpu/) support, to allow sharing of the MIG partition of A100 or H100 GPUs between workloads.
  - Full-featured Helm Chart for distinct maintainability.
  - Hardening the security of the solution, dropping the need to execute commands inside the Pods in particular.
  - Implementing support for JSON in client tools to address automation cases.
  - Redefining the logging for better observability and troubleshooting.
  - Expanding Prometheus metrics with allocation stats.

It is expected that MortalGPU project continue to diverge more from MetaGPU during further development.

## How MortalGPU works?

Several MortalGPU components are working together to provide GPUs sharing experience for end-user workloads. 

Implementation is based on [Nvidia Container Toolkit](https://github.com/NVIDIA/nvidia-container-toolkit)
and [go-nvml](https://github.com/NVIDIA/go-nvml) library.

### Device Plugin

MortalGPU looks for available GPUs and represents 1 GPU device (physical or MIG partition) with a configurable amount of meta-devices.
It registers as the device plugin in Kubernetes managing [extended resources](https://kubernetes.io/docs/concepts/configuration/manage-resources-containers/#managing-extended-resources) in meta-devices capacity (e.g. `320` of `mortalgpu/v100`).

These meta-devices are used by Kubernetes scheduling, allowing (from a scheduling point of view) to allocate fraction of GPU to the workload. MortalGPU then ensures that workload is only given access to the physical device, shares belong to, configuring the container runtime appropriately.

The device plugin part is also responsible for making available the `mgctl` - the command line tool that provides GPU usage observability on the container level.

### Workload monitoring and memory enforcement

MortalGPU monitoring loop is doing the mapping of GPU processes to containers running in Kuberentes Pods. 

This information is used to provide Kubernets-aware observability in general and container-scoped resource usage in particular for both end-users and admins.

Furthermore, the GPU memory usage of the processes in the container is then matched against defined allocation limits. MortalGPU enforces the memory limits by killing the processes.

### Observability

MortalGPU implements gRPC interface for providing the monitoring data to both `mgctl` tool and Prometheus exporter.

[Prometheus exporter](./cmd/mgex) provides both GPU Device level and Kubernetes-aware Process level metrics allowing to monitor GPU consumption of specific workloads in the shared environment. 


## Installing MortalGPU

The prerequisite of using the MortalGPU is the installation of [Nvidia Container Toolkit](https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/latest/install-guide.html) on the Kubernetes nodes.

Use provided [Helm Chart](https://artifacthub.io/packages/helm/mortalgpu/mortalgpu) to install MortalGPU.

You have to define the GPU sharing configuration via the `config.deviceSharing` in the `values.yaml`. It is recommended to define shares based on GPU memory (e.g. in 100MB units).
If there are several GPUs on a Kubernetes host, then the number of units for each GPU must be chosen so that the size of one unit is the same
for all the shared GPUs, otherwise memory enforcement might have unexpected behavior.

For example, defining `320` shares for all worker nodes with V100 32GB will look like this:
```
  deviceSharing:
    - resourceName: mortalgpu/v100
      metagpusPerGpu: 320
      uuid: []
      migid: []
```

Or `400` shares for A100 shared 40G MIG partition:
```
  deviceSharing:
    - resourceName: mortalgpu/a100-shared
      metagpusPerGpu: 400
      uuid: []
      migid:
       - 2
```

Please follow the [Helm Chart documentation](https://artifacthub.io/packages/helm/mortalgpu/mortalgpu) for information about possible configuration values.

## Using MortalGPU resources in workloads

MortalGPU as *Device Plugin* just defines *extended resources* based on the sharing configuration.

To request the GPU resource under the MortalGPU management just use the normal way of `resources` definitions.

For example, you can see in the example output of the `kubectl describe nodes` MortalGPU extended resources are defined like this:
```
Capacity:
  cpu:                     96
  mortalgpu/a100-10g:      200
  mortalgpu/a100-20g:      200
  mortalgpu/a100-shared:   400
  memory:                  1056011536Ki
  pods:                    220
```

In the container specification 4GB of GPU RAM can be requested like this:

```yaml
    resources:
      limits:
        cpu: "8"
        mortalgpu/a100-shared: "40"
        memory: "107374182400"
      requests:
        cpu: 100m
        mortalgpu/a100-shared: "40"
        memory: 524288k
```


However, Kubernetes [does not allow](https://kubernetes.io/docs/concepts/configuration/manage-resources-containers/#consuming-extended-resources) overcommit of the extended resources.
So `requests` and `limits` must be equal if both are present in a container spec.

To work around this and allow GPU memory overcommit, MortalGPU handles the specific Pod annotation `gpu-mem-limit.<resource name>`. 
For example, the following annotation allows to overcommit GPU memory up to 8GB:

```yaml
metadata:
  annotations:
    gpu-mem-limit.mortalgpu/a100-shared: "80"
```
