FROM docker.io/library/golang:1.22-bullseye as builder
ARG BUILD_SHA
ARG BUILD_VERSION
ARG GIT_BUILD_TIME
ARG GIT_COMMIT
ARG GIT_PROJECT
WORKDIR /root/.go/src/mortalgpu
# COPY go-nvml go-nvml
COPY go.mod go.mod
COPY go.sum go.sum
RUN go mod download
COPY cmd cmd
COPY pkg pkg
COPY gen gen
RUN go build \
     -ldflags="-extldflags=-Wl,-z,lazy -s -w -X 'main.Build=${BUILD_SHA}' -X 'main.Version=${BUILD_VERSION}'" \
     -o mgdp cmd/mgdp/main.go
RUN go build \
     -ldflags="-X 'main.Build=${BUILD_SHA}' -X 'main.Version=${BUILD_VERSION}'" \
     -o mgctl cmd/mgctl/*.go
RUN go build \
     -ldflags="-X 'main.Build=${BUILD_SHA}' -X 'main.Version=${BUILD_VERSION}'" \
     -o mgex cmd/mgex/*.go

FROM docker.io/library/rockylinux:9.3-minimal
LABEL io.k8s.display-name="MortalGPU Device Plugin"
LABEL name="MortalGPU Device Plugin"
LABEL vendor="maxiv.lu.se"
LABEL version="N/A"
LABEL release="N/A"
LABEL summary="MortalGPU device plugin for overcommitted GPU sharing"
LABEL description="See summary"
LABEL git-commit="${GIT_COMMIT}"
LABEL git-build-time="${GIT_BUILD_TIME}"
LABEL git-project="${GIT_PROJECT}"
COPY --from=builder /root/.go/src/mortalgpu/mgdp /usr/bin/mgdp
COPY --from=builder /root/.go/src/mortalgpu/mgctl /usr/bin/mgctl
COPY --from=builder /root/.go/src/mortalgpu/mgex /usr/bin/mgex

